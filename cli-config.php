<?php

require 'vendor/autoload.php';

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use app\shared\Doctrine;
use app\shared\Helper;

// the connection configuration
$dbConfig = Helper::loadConf("doctrine")["connection"];
$entityManager = Doctrine::getEntityManager($dbConfig, true);

return ConsoleRunner::createHelperSet($entityManager);