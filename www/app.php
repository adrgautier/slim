<?php
//autoload
require "../vendor/autoload.php";

use app\shared\Helper;

define("MODE","prod");
define("__APP__",Helper::getAppDir());

//  CONFIG
$config = Helper::loadConf(MODE);
$config["doctrine"] = Helper::loadConf("doctrine");

//  APP init
$app = new \Slim\App($config);

//  Container
include(__APP__."/Controller/container.php");

//  Route
include(__APP__."/Controller/route.php");

//  APP run
$app->run();