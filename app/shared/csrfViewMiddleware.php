<?php
$csrfViewMiddleware = function($request, $response, $next) {

$nameKey = $this->csrf->getTokenNameKey();
$valueKey = $this->csrf->getTokenValueKey();
$csrf = [$nameKey => $request->getAttribute($nameKey), $valueKey => $request->getAttribute($valueKey)];

$this->view->getEnvironment()->addGlobal("csrf", $csrf);

$response = $next($request, $response);

return $response;
};