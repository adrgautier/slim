<?php
$csrfCookieMiddleware = function($request, $response, $next) {

$nameKey = $this->csrf->getTokenNameKey();
$valueKey = $this->csrf->getTokenValueKey();

//cookies valid during 1 hour on the whole domain
setcookie($nameKey,$request->getAttribute($nameKey),time()+3600, "/");
setcookie($valueKey,$request->getAttribute($valueKey), time()+3600, "/");

$response = $next($request, $response);

return $response;
};