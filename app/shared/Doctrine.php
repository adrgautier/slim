<?php

namespace app\shared;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class Doctrine{

    /**
     * @param array $connectionOptions DB credidentials
     * @param boolean $devMode is the mode dev ?
     * @return EntityManager
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     */
    static function getEntityManager(array $connectionOptions, $devMode){
        $paths = array(Helper::getAppDir().'/Model/Entity');

        $config = Setup::createAnnotationMetadataConfiguration($paths, $devMode);

        $entityManager = EntityManager::create($connectionOptions, $config);

        //ENUM Mapping to Varchar
        $entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping("enum","string");
        $entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping("binary","string");

        return $entityManager;
    }
}
