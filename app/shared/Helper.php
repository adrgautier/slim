<?php

namespace app\shared;

use Slim\Http\Request;
use Symfony\Component\Yaml\Yaml;


class Helper
{
    static function getAppDir(){
        return substr(__DIR__, 0, -7);
    }

    /*
    static function getEntityDefinition($entity){
        return trim(__DIR__, "/shared")."Model/Entity/app.Model.Entity.".$entity.".dcm.yml";
    }*/

    static function loadYaml($path){
        return Yaml::parse(file_get_contents(static::getAppDir()."/$path"));
    }

    static function loadConf($confname){
        return Yaml::parse(file_get_contents(static::getAppDir()."/conf/$confname.yml"));
    }

    static function getRootUrl(Request $request){
        return substr($request->getUri(), 0, -strlen($request->getRequestTarget()));
    }

    static function getUrl($path, Request $request, $mode = "prod"){
        return self::getRootUrl($request).($mode=="dev"?"/app_dev.php":"").$path;
    }
}