<?php

use app\shared\Helper;
use Slim\Http\Request;
use Slim\Http\Response;

$sessionUserMiddleware = function(Request $request,Response $response, $next) {
    //no user logged
    if ($this->session->get("user") == null) {
        $response = $response->withStatus(401, "Connection required.");
        if($request->isXhr()) {
            return $response;
        }
        else {
            if(MODE == "dev"){
                $requestTarget = substr($request->getRequestTarget(), 12);
            }
            else{
                $requestTarget = $request->getRequestTarget();
            }
            $url = Helper::getUrl("/admin/signin/?$requestTarget", $request, MODE);
            return $response->withRedirect($url);
        }
    } //someone logged
    else {
        $response = $next($request, $response);

    }
return $response;
};