<?php
use Slim\Http\Response;
use Slim\Container;


/**
 * @param array $page
 * @param Response $response
 * @return Response
 */
function renderHomePage(array $page, Response $response, Container $c){
    $content = $c->resource->content->readFromPageId($page['id']);
    $sectionBlocs = $c->resource->bloc->readAllFromParentInstanceIdWithCategory($content['id'], 'section');

    //prerender
    $response = $c->partialView->render($response,'Front/page/home/preTemplate.html.twig',['page'=>$page, 'content'=>$content]);
    //loop
    foreach($sectionBlocs as $bloc){
        switch($bloc['template']){
            default:
            case 'banner_section':
                renderBannerSectionBloc($bloc, $response, $c);
                break;
            case 'text_section':
                renderTextSectionBloc($bloc, $response, $c);
                break;
            /*case 'vignette_section':
                renderVignetteSectionBloc($bloc, $response, $c);
                break;
*/
        }
    }
    //postrender
    return $c->partialView->render($response,'Front/page/home/postTemplate.html.twig',['page'=>$page, 'content'=>$content]);
}