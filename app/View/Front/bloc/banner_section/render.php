<?php
use Slim\Http\Response;
use Slim\Container;

/**
 * @param array $bloc
 * @param Response $response
 * @return Response
 */
function renderBannerSectionBloc(array $bloc, Response $response, Container $c){
    $content = $c->resource->content->readFromBlocId($bloc['id']);
    $actionBlocs = $c->resource->bloc->readAllFromParentInstanceIdWithCategory($content['id'], 'action');
    $hasActionBlocs = count($actionBlocs)>0?true:false;

    //prerender
    $response = $c->partialView->render($response,'Front/bloc/banner_section/preTemplate.html.twig',['bloc'=>$bloc, 'content'=>$content, 'hasActionBlocs' => $hasActionBlocs]);
    //loop
    if($hasActionBlocs){
        foreach($actionBlocs as $bloc){
            switch($bloc['template']){
                default:
                        renderActionBloc($bloc, $response, $c);
                    break;

            }
        }
    }
    //postrender
    return $c->partialView->render($response,'Front/bloc/banner_section/postTemplate.html.twig',['bloc'=>$bloc, 'content'=>$content, 'hasActionBlocs' => $hasActionBlocs]);
}