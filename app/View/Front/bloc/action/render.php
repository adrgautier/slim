<?php
use Slim\Http\Response;
use Slim\Container;


/**
 * @param array $bloc
 * @param Response $response
 * @return Response
 */
function renderActionBloc(array $bloc, Response $response, Container $c){
    $content = $c->resource->content->readFromBlocId($bloc['id']);

    return $c->partialView->render($response,'Front/bloc/action/template.html.twig',['bloc'=>$bloc, 'content'=>$content]);
}