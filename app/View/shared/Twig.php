<?php
namespace app\View\shared;

use Psr\Http\Message\ResponseInterface;

class Twig extends \Slim\Views\Twig
{
    /**
     * Append rendered template to response's body
     *
     * @param ResponseInterface $response
     * @param  string $template Template pathname relative to templates directory
     * @param  array $data Associative array of template variables
     * @return ResponseInterface
     */
    public function render(ResponseInterface $response, $template, $data = [])
    {
        $response->getBody()->write($response->getBody()->getContents().$this->fetch($template, $data));

        return $response;
    }
}