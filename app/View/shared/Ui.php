<?php
namespace app\View\shared;

use Symfony\Component\Yaml\Yaml;

class Ui
{
    static function getUiDir(){
        return substr(__DIR__, 0, -12)."/ui";
    }

    static function get($path,$bundle,$lang){
        $path_array = explode("/",$path);
        $string = Yaml::parse(file_get_contents(static::getUiDir()."/$bundle.$lang.yml"));
        foreach($path_array as $path_level){
            if(isset($string[$path_level])){
                $string = $string[$path_level];
            }
            elseif(isset($string["default"])){
                $string = $string["default"];
            }
            else{
                return null;
            }
        }
        return $string;
    }
}