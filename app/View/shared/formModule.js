var formModule = angular.module('formModule', []);

formModule.directive("formField", function(){
    return {
        link: function(scope, element, attrs){
            var key = attrs.ngModel;

            //replace dots by undescores
            String.prototype.d2us = function(){
                return this.replace(/\./g,"_");
            };

            scope.read = function(keyString){
                var that = this;
                // http://stackoverflow.com/a/33397682
                return keyString.split('.').reduce(function(a, b) {
                    if(a !== undefined){
                        return a[b];
                    }
                    else{
                        return undefined;
                    }
                }, that);
            };

            //Input (default)
            element.on("refresh input focus blur", function(){
                var holder = document.getElementById("hld_"+key.d2us()),
                    input = element[0];

                switch(input.getAttribute("type")){
                    default:
                    case "text":
                        var state = "empty";

                        if(input === document.activeElement){
                            state = "focused";
                        }

                        /*if(scope.form !== undefined){
                            var value = scope.form[key];
                            if(typeof value == "string" && value !== ""){
                                state = "filled";
                            }
                        }*/
                        if(scope.read(key) !== undefined) {
                            var value = scope.read(key);
                            if (typeof value == "string" && value !== "") {
                                state = "filled";
                            }
                        }


                        holder.setAttribute("data-state",state);

                        break;
                    case "password":
                        var state = "empty";

                        if(input === document.activeElement){
                            state = "focused";
                        }

                        /*if(scope.form !== undefined){

                            if(key.search("current") !== -1){
                                var k = key.slice(0, -8);
                                var value = (scope.form[k] !== undefined) ? scope.form[k][0] : "";
                            }
                            else if(key.search("repeat") !== -1){
                                var k = key.slice(0, -7);
                                var value = (scope.form[k] !== undefined) ? scope.form[k][2] : "";
                            }
                            else{
                                var value = (scope.form[key] !== undefined) ? scope.form[key][1] : "";
                            }

                            if(typeof value == "string" && value !== ""){
                                state = "filled";
                            }
                        }*/

                        if(scope.read(key) !== undefined){
                            var value = scope.read(key);
                            if(typeof value == "string" && value !== ""){
                                state = "filled";
                            }
                        }

                        holder.setAttribute("data-state",state);
                        break;
                    case "checkbox":
                        break;

                }


            });

            //Select, Input Checkbox
            element.on("refresh change", function(){
                var holder = document.getElementById("hld_"+key.d2us()),
                    input = element[0];
                if(input.tagName == "SELECT"){
                    /*if (scope.form !== undefined) {
                        var value = scope.form[key];
                        if (value) {
                            holder.setAttribute("data-state", "filled");
                        }
                        else {
                            holder.removeAttribute("data-state");
                        }
                    }*/
                    if (scope.read(key) !== undefined) {
                        var value = scope.read(key);
                        if (value) {
                            holder.setAttribute("data-state", "filled");
                        }
                        else {
                            holder.removeAttribute("data-state");
                        }
                    }
                }
                else {
                    switch (input.getAttribute("type")) {
                        default:
                            break;
                        case "checkbox":
                            /*if (scope.form !== undefined) {
                                var value = scope.form[key];
                                if (value == true) {
                                    holder.setAttribute("data-state", "checked");
                                }
                                else {
                                    holder.removeAttribute("data-state");
                                }
                            }*/
                            if (scope.read(key) !== undefined) {
                                var value = scope.read(key);
                                if (value == true) {
                                    holder.setAttribute("data-state", "checked");
                                }
                                else {
                                    holder.removeAttribute("data-state");
                                }
                            }
                            break;
                    }
                }
            })
        }
    }
});