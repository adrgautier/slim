<?php
namespace app\View\shared;

use MatthiasMullie\Minify;

class Helper
{
    private $mode;

    public function __construct($mode)
    {
        //set MODE
        $this->mode = $mode;
    }

    public function asset($file, $bundle){
        $filename = pathinfo($file, PATHINFO_FILENAME);
        $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));

        $bundle = strtolower($bundle);

        switch($this->mode){
            case "dev":
                return "/assets_dev/$bundle/$extension/$filename.$extension";
                break;
            case "prod":
            default:
                return "/assets/$bundle/$extension/$filename.min.$extension";
                break;

        }
    }

    public function route($uri){
        switch($this->mode){
            case "dev":
                return "/app_dev.php$uri";
                break;
            case "prod":
            default:
                return "$uri";
                break;

        }
    }

    public function getMode(){
        return $this->mode;
    }

    public function minCSS($css)
    {
        switch($this->mode){
            case "dev":
                return $css;
                break;
            case "prod":
            default:
                $minifier = new Minify\CSS($css);
                return $minifier->minify();
                break;
        }
    }

    public function minJS($js)
    {
        switch($this->mode){
            case "dev":
                return $js;
                break;
            case "prod":
            default:
                $minifier = new Minify\JS($js);
                return $minifier->minify();
                break;
        }
    }
}