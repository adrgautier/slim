userModule.controller('updateController', ['$scope', '$state', 'UserService', '$templateCache', function($scope, $state, UserService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    //fill form
    UserService.read(
        $state.params.id,
        function successCallback(response) {
            $scope.user = response.data;
            $scope.user.password = {};
            //force interface refresh
            setTimeout(function(){
                for(var key in $scope.user){
                    if(document.getElementById("npt_user_"+key) !== null){
                        var field = document.getElementById("npt_user_"+key);
                        field.dispatchEvent(new Event("refresh"));
                    }
                }
            },0);

            $scope.submit = function(){
                UserService.update($state.params.id, $scope.user,
                    function successCallback(response) {
                        console.log(response.data);
                    },
                    function errorCallback(response) {
                        console.log(response.statusText)
                    });
            }
        },
        function errorCallback(){
            $state.go('updateIndex');
        }
    );


}]);