userModule.controller('indexController', ['$scope', 'UserService', function($scope, UserService) {
    $scope.status.items = [];
    UserService.readAll(
        function successCallback(response){
            $scope.users = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);