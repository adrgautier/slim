userModule.controller('readIndexController', ['$scope', '$state', 'UserService', function($scope, $state, UserService) {
    $scope.status.items = [];
    UserService.readAll(
        function successCallback(response){
            $scope.users = response.data;
        },
        function errorCallback(response) {
            console.log(response.statusText)
        }
    );
}]);