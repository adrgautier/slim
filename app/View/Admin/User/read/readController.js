userModule.controller('readController', ['$scope', '$state', 'UserService', function($scope, $state, UserService) {
    $scope.status.items = [];
    UserService.read(
        $state.params.id,
        function successCallback(response){
            $scope.user = response.data;
        },
        function errorCallback(){
            $state.go('readIndex');
        }
    );
}]);