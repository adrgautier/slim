userModule.controller('deleteController', ['$scope', '$state', 'UserService', '$templateCache', function($scope, $state, UserService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    UserService.read(
        $state.params.id,
        function successCallback(response) {
            $scope.user = response.data;
            $scope.submit = function () {
                UserService.delete($scope.user.id,
                    function successCallback(response){
                        $scope.status.success(response);
                    },
                    function errorCallback(response){
                        $scope.status.error(response);
                    }
                );
            };
        },
        function errorCallback(){
            $state.go('deleteIndex');
        }
    );


}]);