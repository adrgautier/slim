userModule.controller('deleteIndexController', ['$scope', '$state', 'UserService', '$templateCache', function($scope, $state, UserService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    UserService.readAll(
        function successCallback(response){
            $scope.users = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);