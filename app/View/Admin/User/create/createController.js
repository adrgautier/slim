userModule.controller('createController', ['$scope', '$state', 'UserService', '$templateCache', function($scope, $state, UserService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    $scope.user = {};
    $scope.user.password = {};

    $scope.submit = function(){
        UserService.create($scope.user,
            function successCallback(response){
                $scope.status.success(response);
            },
            function errorCallback(response){
                $scope.status.error(response);
            }
        );
    }
}]);