categoryModule.controller('updateIndexController', ['$scope', '$state', 'CategoryService', '$templateCache', function($scope, $state, CategoryService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    CategoryService.readAll(
        function successCallback(response){
            $scope.categories = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);