categoryModule.controller('updateController', ['$scope', '$state', 'CategoryService', 'TypeService', '$templateCache', function($scope, $state, CategoryService, TypeService, $templateCache) {
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    var currentCategory;
    //fill form
    CategoryService.read(
        $state.params.id,
        function successCallback(response) {
            $scope.category = response.data;
            $scope.category.createdProperties = [];
            $scope.category.deletedProperties = [];

            $scope.modes = [];
            CategoryService.readAll(function successCallback(response){
                var modes = response.data;
                for(var i in modes){
                    var mode = modes[i];
                    mode.entity = "category";
                    $scope.modes.push(mode);
                }

                TypeService.readAll(function successCallback(response){
                    var modes = response.data;
                    for(var i in modes){
                        var mode = modes[i];
                        mode.entity = "type";
                        $scope.modes.push(mode);
                    }


                    for(var key in $scope.category.properties){
                        var property = $scope.category.properties[key];

                        $scope.modes.forEach(function(mode){
                            if(mode.entity == property["mode"]["entity"] && mode.id == property["mode"]["id"]){
                                property.mode = mode;
                            }
                        });

                        property.delete = function(){
                            $scope.category.deletedProperties.push(this);
                            $scope.category.properties.splice($scope.category.properties.indexOf(this),1);
                        };

                        //$scope.category.properties[key] = property;
                    }
                });
            });

            $scope.createProperty = function(){
                var createdProperty = {};
                createdProperty.delete = function(){
                    $scope.category.createdProperties.splice($scope.category.createdProperties.indexOf(this),1);
                };
                $scope.category.createdProperties.push(createdProperty);
            };


            $scope.submit = function(){
                CategoryService.update($state.params.id, $scope.category,
                    function successCallback(response){
                        $scope.status.success(response);
                    },
                    function errorCallback(response){
                        $scope.status.error(response);
                    });
            }
        },
        function errorCallback(){
            $state.go('updateIndex');
        }
    );


}]);