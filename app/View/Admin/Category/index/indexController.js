categoryModule.controller('indexController', ['$scope', 'CategoryService', function($scope, CategoryService) {
    $scope.status.items = [];
    CategoryService.readAll(
        function successCallback(response){
            $scope.categories = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);