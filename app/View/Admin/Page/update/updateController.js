pageModule.controller('updateController', ['$scope', '$state', 'PageService', '$templateCache', function($scope, $state, PageService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    //fill form
    PageService.read(
        $state.params.id,
        function successCallback(response) {
            $scope.page = response.data;
            //force interface refresh
            setTimeout(function(){
                for(var key in $scope.page){
                    if(document.getElementById("npt_page_"+key) !== null){
                        var field = document.getElementById("npt_page_"+key);
                        field.dispatchEvent(new Event("refresh"));
                    }
                }
            },0);

            $scope.submit = function(){
                PageService.update($state.params.id, $scope.page,
                    function successCallback(response) {
                        console.log(response.data);
                    },
                    function errorCallback(response) {
                        console.log(response.statusText)
                    });
            }
        },
        function errorCallback(){
            $state.go('updateIndex');
        }
    );


}]);