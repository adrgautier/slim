pageModule.controller('indexController', ['$scope', 'PageService', function($scope, PageService) {
    $scope.status.items = [];
    PageService.readAll(
        function successCallback(response){
            $scope.pages = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);