pageModule.controller('readController', ['$scope', '$state', 'PageService', function($scope, $state, PageService) {
    $scope.status.items = [];
    PageService.read(
        $state.params.id,
        function successCallback(response){
            $scope.page = response.data;
        },
        function errorCallback(){
            $state.go('readIndex');
        }
    );
}]);