pageModule.controller('readIndexController', ['$scope', '$state', 'PageService', function($scope, $state, PageService) {
    $scope.status.items = [];
    PageService.readAll(
        function successCallback(response){
            $scope.pages = response.data;
        },
        function errorCallback(response) {
            console.log(response.statusText)
        }
    );
}]);