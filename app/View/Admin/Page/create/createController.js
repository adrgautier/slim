pageModule.controller('createController', ['$scope', '$state', 'PageService', '$templateCache', function($scope, $state, PageService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    $scope.page = {};
    $scope.page.activation = false;

    $scope.submit = function(){
        PageService.create($scope.page,
            function successCallback(response){
                $scope.status.success(response);
            },
            function errorCallback(response){
                $scope.status.error(response);
            }
        );
    }
}]);