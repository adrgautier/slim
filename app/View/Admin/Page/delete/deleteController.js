pageModule.controller('deleteController', ['$scope', '$state', 'PageService', '$templateCache', function($scope, $state, PageService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    PageService.read(
        $state.params.id,
        function successCallback(response) {
            $scope.page = response.data;
            $scope.submit = function () {
                PageService.delete($scope.page.id,
                    function successCallback(response){
                        $scope.status.success(response);
                    },
                    function errorCallback(response){
                        $scope.status.error(response);
                    }
                );
            };
        },
        function errorCallback(){
            $state.go('deleteIndex');
        }
    );


}]);