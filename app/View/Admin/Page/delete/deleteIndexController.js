pageModule.controller('deleteIndexController', ['$scope', '$state', 'PageService', '$templateCache', function($scope, $state, PageService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    PageService.readAll(
        function successCallback(response){
            $scope.pages = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);