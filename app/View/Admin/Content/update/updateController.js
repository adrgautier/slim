contentModule.controller('updateController', ['$scope', '$state', 'ContentService', '$templateCache', function($scope, $state, ContentService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    var currentContent;
    //fill form
    ContentService.read(
        $state.params.category,
        $state.params.id,
        function successCallback(response) {
            $scope.content = response.data;

            //fill field
            setTimeout(function(){
                for(var key in $scope.content){
                    if(document.getElementById("npt_content_"+key) !== null){

                        var field = document.getElementById("npt_content_"+key),
                            type = field.getAttribute("type");

                        if(field.tagName == "SELECT"){
                            field.value = $scope.content[key];

                        }
                        //force interface refresh
                        field.dispatchEvent(new Event("refresh"));
                    }
                }
            },0);

            $scope.submit = function(){
                ContentService.update($state.params.category, $state.params.id, $scope.content,
                    function successCallback(response){
                        $scope.status.success(response);
                    },
                    function errorCallback(response){
                        $scope.status.error(response);
                    });
            }
        },
        function errorCallback(){
            $state.go('updateIndex');
        }
    );


}]);