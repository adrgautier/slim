contentModule.controller('indexController', ['$scope', '$state', 'ContentService', function($scope, $state, ContentService) {
    $scope.status.items = [];
    ContentService.readAll(
        $state.params.category,
        function successCallback(response){
            $scope.contents = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);