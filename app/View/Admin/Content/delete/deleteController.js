contentModule.controller('deleteController', ['$scope', '$state', 'ContentService', '$templateCache', function($scope, $state, ContentService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    ContentService.read(
        $state.params.category,
        $state.params.id,
        function successCallback(response) {
            $scope.content = response.data;
            $scope.submit = function () {
                ContentService.delete(
                    $state.params.category,
                    $scope.content.id,
                    function successCallback(response){
                        $scope.status.success(response);
                    },
                    function errorCallback(response){
                        $scope.status.error(response);
                    }
                );
            };
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );


}]);