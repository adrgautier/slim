contentModule.controller('deleteIndexController', ['$scope', '$state', 'ContentService', '$templateCache', function($scope, $state, ContentService, $templateCache) {
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    ContentService.readAll(
        $state.params.category,
        function successCallback(response){
            $scope.contents = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);