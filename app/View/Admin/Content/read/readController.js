contentModule.controller('readController', ['$scope', '$state', 'ContentService', function($scope, $state, ContentService) {
    $scope.status.items = [];
    ContentService.read(
        $state.params.category,
        $state.params.id,
        function successCallback(response){
            $scope.content = response.data;
        },
        function errorCallback(){
            $state.go('readIndex');
        }
    );
}]);