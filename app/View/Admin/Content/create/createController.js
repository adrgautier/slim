contentModule.controller('createController', ['$scope', '$state', 'ContentService', '$templateCache', function($scope, $state, ContentService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    $scope.content = {};

    $scope.submit = function(){
        ContentService.create($state.params.category, $scope.content,
            function successCallback(response){
                $scope.status.success(response);
            },
            function errorCallback(response){
                $scope.status.error(response);
            });
    }
}]);