blocModule.controller('updateController', ['$scope', '$state', 'BlocService', '$templateCache', function($scope, $state, BlocService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    //fill form
    BlocService.read(
        $state.params.id,
        function successCallback(response) {
            $scope.bloc = response.data;
            //force interface refresh
            setTimeout(function(){
                for(var key in $scope.bloc){
                    if(document.getElementById("npt_bloc_"+key) !== null){
                        var field = document.getElementById("npt_bloc_"+key);
                        field.dispatchEvent(new Event("refresh"));
                    }
                }
            },0);

            $scope.submit = function(){
                BlocService.update($state.params.id, $scope.bloc,
                    function successCallback(response) {
                        console.log(response.data);
                    },
                    function errorCallback(response) {
                        console.log(response.statusText)
                    });
            }
        },
        function errorCallback(){
            $state.go('updateIndex');
        }
    );


}]);