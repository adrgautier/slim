blocModule.controller('deleteIndexController', ['$scope', '$state', 'BlocService', '$templateCache', function($scope, $state, BlocService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    BlocService.readAll(
        function successCallback(response){
            $scope.blocs = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);