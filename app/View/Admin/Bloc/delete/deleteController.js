blocModule.controller('deleteController', ['$scope', '$state', 'BlocService', '$templateCache', function($scope, $state, BlocService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    BlocService.read(
        $state.params.id,
        function successCallback(response) {
            $scope.bloc = response.data;
            $scope.submit = function () {
                BlocService.delete($scope.bloc.id,
                    function successCallback(response){
                        $scope.status.success(response);
                    },
                    function errorCallback(response){
                        $scope.status.error(response);
                    }
                );
            };
        },
        function errorCallback(){
            $state.go('deleteIndex');
        }
    );


}]);