blocModule.controller('createController', ['$scope', '$state', 'BlocService', '$templateCache', function($scope, $state, BlocService, $templateCache) {
    $scope.status.items = [];
    //remove the template from cache (for CSRF token reload)
    $templateCache.remove($state.current.loadedTemplateUrl);

    $scope.bloc = {};
    $scope.bloc.activation = false;

    $scope.submit = function(){
        BlocService.create($scope.bloc,
            function successCallback(response){
                $scope.status.success(response);
            },
            function errorCallback(response){
                $scope.status.error(response);
            }
        );
    }
}]);