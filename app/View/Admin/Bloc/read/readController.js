blocModule.controller('readController', ['$scope', '$state', 'BlocService', function($scope, $state, BlocService) {
    $scope.status.items = [];
    BlocService.read(
        $state.params.id,
        function successCallback(response){
            $scope.bloc = response.data;
        },
        function errorCallback(){
            $state.go('readIndex');
        }
    );
}]);