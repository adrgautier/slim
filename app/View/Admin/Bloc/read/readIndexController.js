blocModule.controller('readIndexController', ['$scope', '$state', 'BlocService', function($scope, $state, BlocService) {
    $scope.status.items = [];
    BlocService.readAll(
        function successCallback(response){
            $scope.blocs = response.data;
        },
        function errorCallback(response) {
            console.log(response.statusText)
        }
    );
}]);