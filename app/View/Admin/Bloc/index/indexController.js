blocModule.controller('indexController', ['$scope', 'BlocService', function($scope, BlocService) {
    $scope.status.items = [];
    BlocService.readAll(
        function successCallback(response){
            $scope.blocs = response.data;
        },
        function errorCallback(response){
            $scope.status.error(response);
        }
    );
}]);