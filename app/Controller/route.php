<?php

use Slim\Http\Request;
use Slim\Http\Response;

include(__APP__."/shared/sessionUserMiddleware.php");
include(__APP__."/shared/csrfCookieMiddleware.php");
include(__APP__."/shared/csrfViewMiddleware.php");






//
//  API (with csrf)
//

$app->group('/api', function () {
    include(__APP__."/Controller/Api/route.php");
})
    ->add($sessionUserMiddleware)
    ->add($csrfCookieMiddleware)
    ->add($container->csrf)
    /*->add(function(Request $request, Response $response, $next) {

        //print_r($request);

        $parsed = json_encode($request->getParsedBody());
        $params = json_encode($request->getParams());
        $query = json_encode($request->getQueryParams());

        $response = $response->withAddedHeader('ParsedBody',$parsed);
        $response = $response->withAddedHeader('Params',$params);
        $response = $response->withAddedHeader('Query',$query);



        $response = $next($request, $response);

        return $response;
    })*/
    ->add(new \RKA\SessionMiddleware(['name' => 'AdminSession']));


//
//  Admin
//

$app->group('/admin', function () {
    include(__APP__."/Controller/Admin/route.php");
})
    ->add($csrfCookieMiddleware)
    ->add($csrfViewMiddleware)
    ->add($container->csrf)
    ->add(new \RKA\SessionMiddleware(['name' => 'AdminSession']));

//
//  Front
//
//templates:
include(__APP__.'/View/Front/page/home/render.php');
include(__APP__.'/View/Front/bloc/banner_section/render.php');
include(__APP__.'/View/Front/bloc/text_section/render.php');
//include(__APP__.'/View/Front/bloc/vignette_section/render.php');
include(__APP__.'/View/Front/bloc/action/render.php');

$app->group('/front', function () {
    $this->get('/{url}[/]', function(Request $request, Response $response, $args) {
        $page = $this->resource->page->readFromUrl($args['url']);
        switch($page['template']){
            default:
            case 'home':
                renderHomePage($page, $response, $this);
                break;
        }
        return $response;
    });
});