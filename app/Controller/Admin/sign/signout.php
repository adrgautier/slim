<?php
use RKA\Session;
use app\shared\Helper;

$this->get('/', function ($request, $response, $args) {
    //if someone logged
    if($this->session->get("user") != null){
        //$this->session->set("user", null);
        Session::destroy();
    }

    //redirect to signin
    $url = Helper::getUrl("/admin/signin/", $request, MODE);
    return $response->withRedirect($url);
});