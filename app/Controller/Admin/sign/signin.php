<?php
use app\shared\Helper;
use app\View\shared\Ui;

use Slim\Http\Request;
use Slim\Http\Response;
$this->get('/', function (Request $request, Response $response, $args) {
    //no user logged
    if($this->session->get("user") == null){
        $requestTarget = isset(array_keys($request->getQueryParams())[0])?array_keys($request->getQueryParams())[0]:null;
        return $this->view->render($response, 'Admin/sign/signin.html.twig', ["target" => $requestTarget]);
    }
    //already logged
    else{
        $url = Helper::getUrl("/admin/", $request, MODE);
        return $response->withRedirect($url);
    }

});

$this->post('/', function (Request $request, Response $response, $args) {
    //no user logged
    if($this->session->get("user") == null){
        $mail = $request->getParsedBody()["npt_signin_mail"];
        $password = $request->getParsedBody()["npt_signin_password"];
        $encodedpass = hash('sha256',$password);

        try{
            $user = $this->resource->user->readFromMail($mail);
        }
        catch(Exception $e){

        }

        if(isset($user) && $user != null){
            if($user["password"] == $encodedpass){
                //connexion succeed
                $this->session->set("user", new app\Model\shared\SessionUser($user));

                if(isset($request->getParsedBody()["target"])){;
                    $url = Helper::getUrl($request->getParsedBody()["target"], $request, MODE);
                }
                else{
                    $url = Helper::getUrl("/admin/?signin", $request, MODE);
                }
                return $response->withRedirect($url);
            }
        }

        //connexion failure
        //todo: fill form with previous data
        //setcookie("message","Les identifiants ne semblent pas valides.");
        //$response = $response->withHeader("message","Les identifiants ne semblent pas valides.");


        return $this->view->render($response, 'Admin/sign/signin.html.twig', ["message"=>Ui::get("general/signin/message/fail","admin","fr")]);

    }
    //someone already logged ! want to disconnect ?
    else{
        $url = Helper::getUrl("/admin/signout/", $request, MODE);
        return $response->withRedirect($url);
    }

});