<?php
use Slim\Http\Request;
use Slim\Http\Response;
use app\shared\Helper;

if($this->getContainer()->request->isXhr()){
    //requests from angular
    $this->get('/{category}/index[/[{action}[/]]]', function(Request $request, Response $response, $args) {
        if(isset($args['action'])){
            return $this->view->render($response, 'Admin/Content/index/indexTemplate.html.twig',['action'=>$args['action']]);
        }
        else{
            return $this->view->render($response, 'Admin/Content/index/indexTemplate.html.twig',["category"=>$args["category"]]);
        }

    });

    $this->get('/{category}/create[/]', function(Request $request, Response $response, $args) {
        $fields = $this->resource->category->readFieldsFromName($args["category"]);

        //add mandatory fields
        $fields = array_merge(Helper::loadYaml("Controller/Admin/Content/form.yml"), $fields);

        foreach($fields as $key=>&$field){
            $field["name"] = $key;
        }

        return $this->view->render($response, 'Admin/Content/create/createTemplate.html.twig', [
            "category"=>$args["category"],
            'fields' => $fields
        ]);
    });

    $this->get('/{category}/read[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/Content/read/readTemplate.html.twig',["category"=>$args["category"]]);
    });

    $this->get('/{category}/update[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        $fields = $this->resource->category->readFieldsFromName($args["category"]);

        //add mandatory fields
        $fields = array_merge(Helper::loadYaml("Controller/Admin/Content/form.yml"), $fields);

        foreach($fields as $key=>&$field){
            $field["name"] = $key;
        }

        return $this->view->render($response, 'Admin/Content/update/updateTemplate.html.twig', [
            "category"=>$args["category"],
            'fields' => $fields
        ]);
    });

    $this->get('/{category}/delete[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/Content/delete/deleteTemplate.html.twig',["category"=>$args["category"]]);
    });
}
else{
    $this->get('/{category}/[{action}[/[{id}[/]]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/Content/contentModule.html.twig',["category"=>$args["category"]]);
    });
}

