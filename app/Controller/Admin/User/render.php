<?php

use Slim\Http\Request;
use Slim\Http\Response;
use app\shared\Helper;

if($this->getContainer()->request->isXhr()){
    //requests from angular
    $this->get('/index[/[{action}[/]]]', function(Request $request, Response $response, $args) {
        if(isset($args['action'])){
            return $this->view->render($response, 'Admin/User/index/indexTemplate.html.twig',['action'=>$args['action']]);
        }
        else{
            return $this->view->render($response, 'Admin/User/index/indexTemplate.html.twig');
        }

    });

    $this->get('/create[/]', function(Request $request, Response $response, $args) {
        $fields = Helper::loadYaml("Controller/Admin/User/form.yml");

        foreach($fields as $key=>&$field){
            $field["name"] = $key;
        }

        //remove role options according to user role
        $userRoleWeight = $this->session->user->getRoleWeight();
        foreach($fields["role"]["options"] as $index=>$option){
            if($index+1>=$userRoleWeight){
                unset($fields["role"]["options"][$index]);
            }
        }


        return $this->view->render($response, 'Admin/User/create/createTemplate.html.twig', [
            'fields' => $fields
        ]);
    });

    $this->get('/read[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/User/read/readTemplate.html.twig');
    });

    $this->get('/update[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        $fields = Helper::loadYaml("Controller/Admin/User/form.yml");

        foreach($fields as $key=>&$field){
            $field["name"] = $key;
        }

        //remove role options according to user role
        $userRoleWeight = $this->session->user->getRoleWeight();


        foreach($fields["role"]["options"] as $index=>$option){
            if($index+1>=$userRoleWeight){
                unset($fields["role"]["options"][$index]);
            }
        }


        return $this->view->render($response, 'Admin/User/update/updateTemplate.html.twig', [
            'fields' => $fields
        ]);
    });

    $this->get('/delete[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/User/delete/deleteTemplate.html.twig');
    });
}
else{
    $this->get('/[{action}[/[{id}[/]]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/User/userModule.html.twig',["resource"=>"user"]);
    });
}

