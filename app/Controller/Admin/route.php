<?php
use app\View\shared\Ui;
use Slim\Http\Request;
use Slim\Http\Response;

include(__APP__."/shared/sessionUserMiddleware.php");

$this->group('/signin', function () {
    include(__APP__."/Controller/Admin/sign/signin.php");
});

$this->group('/signout', function () {
    include(__APP__."/Controller/Admin/sign/signout.php");
});

$this->get('/', function (Request $request, Response $response, $args) {
    if(isset($request->getParams()["signin"])){
        $message = Ui::get("general/signin/message/success","admin","fr");
    }
    else{
        $message = null;
    }
    return $this->view->render($response, 'Admin/home/home.html.twig', ["message"=> $message]);
})->add($sessionUserMiddleware);

$this->group('/user', function () {
    include(__APP__."/Controller/Admin/User/render.php");
})->add($sessionUserMiddleware);

$this->group('/category', function () {
    include(__APP__."/Controller/Admin/Category/render.php");
})->add($sessionUserMiddleware);

$this->group('/content', function () {
    include(__APP__."/Controller/Admin/Content/render.php");
})->add($sessionUserMiddleware);

$this->group('/page', function () {
    include(__APP__."/Controller/Admin/Page/render.php");
})->add($sessionUserMiddleware);

$this->group('/bloc', function () {
    include(__APP__."/Controller/Admin/Bloc/render.php");
})->add($sessionUserMiddleware);