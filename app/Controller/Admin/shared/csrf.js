angular.module('csrfModule', ['ngCookies'])
    .factory('csrfCookieService',['$cookies', function($cookies) {

        var service = {};
        service.getTokens = function(){
            return {"csrf_name":$cookies.get("csrf_name"),"csrf_value":$cookies.get("csrf_value")}
        };
        service.getFormData = function(data){
            Object.assign(data,this.getTokens());

            var formData = new FormData();

            for(var field in data){
                if(typeof data[field] == "string" && data[field] !== null){
                    formData.append(field,data[field]);
                }
                else if(data[field] !== [] && data[field] !== {}){
                    formData.append(field,angular.toJson(data[field]));
                }
            }

            return formData;
        };

        return service;
    }]);