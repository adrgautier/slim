<?php

use Slim\Http\Request;
use Slim\Http\Response;
use app\shared\Helper;

if($this->getContainer()->request->isXhr()){
    //requests from angular
    $this->get('/index[/[{action}[/]]]', function(Request $request, Response $response, $args) {
        if(isset($args['action'])){
            return $this->view->render($response, 'Admin/Bloc/index/indexTemplate.html.twig',['action'=>$args['action']]);
        }
        else{
            return $this->view->render($response, 'Admin/Bloc/index/indexTemplate.html.twig');
        }

    });

    $this->get('/create[/]', function(Request $request, Response $response, $args) {
        $fields = Helper::loadYaml("Controller/Admin/Bloc/form.yml");

        //add template selection
        $fields["template"] = $this->resource->blocTemplate->readAllAsField();
        //add relation selection
        $fields["relation"] = $this->resource->relation->readAllAsField();

        foreach($fields as $key=>&$field){
            $field["name"] = $key;
        }

        return $this->view->render($response, 'Admin/Bloc/create/createTemplate.html.twig', [
            'fields' => $fields
        ]);
    });

    $this->get('/read[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/Bloc/read/readTemplate.html.twig');
    });

    $this->get('/update[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        $fields = Helper::loadYaml("Controller/Admin/Bloc/form.yml");

        //add template selection
        $fields["template"] = $this->resource->blocTemplate->readAllAsField();
        //add relation selection
        $fields["relation"] = $this->resource->relation->readAllAsField();

        foreach($fields as $key=>&$field){
            $field["name"] = $key;
        }


        return $this->view->render($response, 'Admin/Bloc/update/updateTemplate.html.twig', [
            'fields' => $fields
        ]);
    });

    $this->get('/delete[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/Bloc/delete/deleteTemplate.html.twig');
    });
}
else{
    $this->get('/[{action}[/[{id}[/]]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/Bloc/blocModule.html.twig',["resource"=>"user"]);
    });
}

