<?php

use Slim\Http\Request;
use Slim\Http\Response;

if($this->getContainer()->request->isXhr()){
    //requests from angular
    $this->get('/index[/[{action}[/]]]', function(Request $request, Response $response, $args) {
        if(isset($args['action'])){
            return $this->view->render($response, 'Admin/Category/index/indexTemplate.html.twig',['action'=>$args['action']]);
        }
        else{
            return $this->view->render($response, 'Admin/Category/index/indexTemplate.html.twig');
        }
    });

    $this->get('/update[/[{id}[/]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/Category/update/updateTemplate.html.twig');
    });
}
else{
    $this->get('/[{action}[/[{id}[/]]]]', function(Request $request, Response $response, $args) {
        return $this->view->render($response, 'Admin/Category/categoryModule.html.twig', ["resource"=>"category"]);
    });
}

