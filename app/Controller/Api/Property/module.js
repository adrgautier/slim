angular.module('PropertyApi',['csrfModule','statusModule'])
    .factory('PropertyService',['$http', 'csrfCookieService', function($http,CsrfCookie) {
        //$http.defaults.headers.post["Content-Type"] = "multipart/form-data";

        var service = {};
            service.read = function(id,successCallback,errorCallback){
                var data = CsrfCookie.getTokens();

                var formData = new FormData();

                for(var field in data){
                    if(Array.isArray(data[field])){
                        formData.append(field,JSON.stringify(data[field]));
                    }
                    else{
                        formData.append(field,data[field]);
                    }
                }

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/api/property/read/'+id,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };
            service.readAll = function(successCallback,errorCallback){
                var data = CsrfCookie.getTokens();

                var formData = new FormData();

                for(var field in data){
                    if(Array.isArray(data[field])){
                        formData.append(field,JSON.stringify(data[field]));
                    }
                    else{
                        formData.append(field,data[field]);
                    }
                }

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/api/property/read',
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };
        service.create = function(data, successCallback,errorCallback){
            // Add Csrf Cookies in datas
            Object.assign(data,CsrfCookie.getTokens());

            var formData = new FormData();

            for(var field in data){
                if(Array.isArray(data[field])){
                    formData.append(field,JSON.stringify(data[field]));
                }
                else{
                    formData.append(field,data[field]);
                }
            }

            $http({
                method: 'POST',
                url: '/app_dev.php/api/property/create',
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };
        service.update = function(id, data, successCallback, errorCallback){
            // Add Csrf Cookies in datas
            Object.assign(data,CsrfCookie.getTokens());

            var formData = new FormData();

            for(var field in data){
                if(Array.isArray(data[field])){
                    formData.append(field,JSON.stringify(data[field]));
                }
                else{
                    formData.append(field,data[field]);
                }
            }

            $http({
                method: 'POST',
                url: '/app_dev.php/api/property/update/'+id,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };
        service.delete = function(id,  successCallback, errorCallback){
            // Add Csrf Cookies in datas
            var data = CsrfCookie.getTokens();

            var formData = new FormData();

            for(var field in data){
                if(Array.isArray(data[field])){
                    formData.append(field,JSON.stringify(data[field]));
                }
                else{
                    formData.append(field,data[field]);
                }
            }

            $http({
                method: 'POST',
                url: '/api/property/delete/'+id,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };

        return service;
    }]);