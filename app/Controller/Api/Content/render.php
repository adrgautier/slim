<?php

/**
 * Rendering the response for the Content API
 */

use Slim\Http\Request;
use Slim\Http\Response;

$this->post('/{category}/read[/{id}[/]]', function(Request $request, Response $response, $args) {
    $response = $response->withHeader('Content-Type', 'application/json; charset=utf-8');
    if(isset($args['id'])){
        try{
            $data = $this->resource->content->read($args['id']);
        }
        catch(Exception $e){
            return $response->withStatus(400,utf8_decode($e->getMessage()));
        }
    }
    else{
        try{
            $data = $this->resource->content->readAll($args['category']);
        }
        catch(Exception $e){
            return $response->withStatus(400,utf8_decode($e->getMessage()));
        }
    }
    echo json_encode($data);
    return $response->withHeader('Content-Type', 'application/json; charset=utf-8');
});

$this->post('/{category}/create[/]', function(Request $request, Response $response, $args) {
    try{
        $body = $request->getParsedBody();
        $files = $request->getUploadedFiles();
        echo json_encode($this->resource->content->create($args['category'], $body, $files));
    }
    catch(Exception $e){
        return $response->withStatus(400,utf8_decode($e->getMessage()));
    }
    return $response->withStatus(200,utf8_decode('Contenu ajouté.'));
});

$this->post('/{category}/update/{id}[/]', function(Request $request, Response $response, $args) {
    try{
        $body = $request->getParsedBody();
        $files = $request->getUploadedFiles();
        echo json_encode($this->resource->content->update($args['id'], $body, $files));
    }
    catch(Exception $e){
        return $response->withStatus(400,utf8_decode($e->getMessage()));
    }
    return $response->withStatus(200,utf8_decode('Contenu modifié.'));
});

$this->post('/{category}/delete/{id}[/]', function(Request $request, Response $response, $args) {
    try{
        echo json_encode($this->resource->content->delete($args['id']));
    }
    catch(Exception $e){
        return $response->withStatus(400,utf8_decode($e->getMessage()));
    }
    return $response->withStatus(200,utf8_decode('Contenu supprimé.'));
});