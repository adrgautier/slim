angular.module('ContentApi',['csrfModule'])
    .factory('ContentService',['$http', 'csrfCookieService', function($http,CsrfCookie) {
        //$http.defaults.headers.post["Content-Type"] = "multipart/form-data";

        var service = {};
            service.read = function(category,id,successCallback,errorCallback){

                var formData = CsrfCookie.getFormData([]);

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/api/content/'+category+'/read/'+id,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };
            service.readAll = function(category,successCallback,errorCallback){

                var formData = CsrfCookie.getFormData([]);

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/api/content/'+category+'/read',
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };
        service.create = function(category, data, successCallback,errorCallback){

            var formData = CsrfCookie.getFormData(data);

            $http({
                method: 'POST',
                url: '/app_dev.php/api/content/'+category+'/create',
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };
        service.update = function(category, id, data, successCallback, errorCallback){

            var formData = CsrfCookie.getFormData(data);

            $http({
                method: 'POST',
                url: '/app_dev.php/api/content/'+category+'/update/'+id,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };
        service.delete = function(category, id, successCallback, errorCallback){

            var formData = CsrfCookie.getFormData([]);

            $http({
                method: 'POST',
                url: '/api/content/'+category+'/delete/'+id,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };

        return service;
    }]);