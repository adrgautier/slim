<?php

$this->group('/user', function(){
    include(__APP__.'/Controller/Api/User/render.php');
});

$this->group('/category', function(){
    include(__APP__.'/Controller/Api/Category/render.php');
});

$this->group('/type', function(){
    include(__APP__.'/Controller/Api/Type/render.php');
});

$this->group('/content', function(){
    include(__APP__.'/Controller/Api/Content/render.php');
});

$this->group('/page', function(){
    include(__APP__.'/Controller/Api/Page/render.php');
});

$this->group('/bloc', function(){
    include(__APP__.'/Controller/Api/Bloc/render.php');
});
