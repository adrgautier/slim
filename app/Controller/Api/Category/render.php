<?php

use Slim\Http\Request;
use Slim\Http\Response;

$this->post('/read[/{id}[/]]', function(Request $request, Response $response, $args) {
    $response = $response->withHeader('Content-Type', 'application/json; charset=utf-8');
    if(isset($args['id'])){
        try{
            $data = $this->resource->category->read($args['id']);
        }
        catch(Exception $e){
            return $response->withStatus(400,utf8_decode($e->getMessage()));
        }
    }
    else{
        try{
            $data = $this->resource->category->readAll();
        }
        catch(Exception $e){
            return $response->withStatus(400,utf8_decode($e->getMessage()));
        }
    }
    echo json_encode($data);
    return $response->withHeader('Content-Type', 'application/json; charset=utf-8');
});

$this->post('/update/{id}[/]', function(Request $request, Response $response, $args) {
    try{
        $body = $request->getParsedBody();
        echo json_encode($this->resource->category->update($args['id'], $body));
    }
    catch(Exception $e){
        print_r($e->getMessage());
        return $response->withStatus(400,utf8_decode($e->getMessage()));
    }
    return $response->withStatus(200,utf8_decode('Catégorie modifiée.'));
});