angular.module('BlocApi',['csrfModule'])
    .factory('BlocService',['$http', 'csrfCookieService', function($http,CsrfCookie) {
        //$http.defaults.headers.post["Content-Type"] = "multipart/form-data";

        var service = {};
            service.read = function(id,successCallback,errorCallback){

                var formData = CsrfCookie.getFormData([]);

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/api/bloc/read/'+id,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };
            service.readAll = function(successCallback,errorCallback){

                var formData = CsrfCookie.getFormData([]);

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/api/bloc/read',
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };
        service.create = function(data, successCallback,errorCallback){

            var formData = CsrfCookie.getFormData(data);

            $http({
                method: 'POST',
                url: '/app_dev.php/api/bloc/create',
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };
        service.update = function(id, data, successCallback, errorCallback){

            var formData = CsrfCookie.getFormData(data);

            $http({
                method: 'POST',
                url: '/app_dev.php/api/bloc/update/'+id,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };
        service.delete = function(id,  successCallback, errorCallback){

            var formData = CsrfCookie.getFormData([]);

            $http({
                method: 'POST',
                url: '/api/bloc/delete/'+id,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };

        return service;
    }]);