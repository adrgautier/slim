angular.module('PageApi',['csrfModule'])
    .factory('PageService',['$http', 'csrfCookieService', function($http,CsrfCookie) {
        //$http.defaults.headers.post["Content-Type"] = "multipart/form-data";

        var service = {};
            service.read = function(id,successCallback,errorCallback){

                var formData = CsrfCookie.getFormData([]);

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/app_dev.php/api/page/read/'+id,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };
            service.readAll = function(successCallback,errorCallback){

                var formData = CsrfCookie.getFormData([]);

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/app_dev.php/api/page/read',
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };
        service.create = function(data, successCallback,errorCallback){

            var formData = CsrfCookie.getFormData(data);

            $http({
                method: 'POST',
                url: '/app_dev.php/api/page/create',
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };
        service.update = function(id, data, successCallback, errorCallback){

            var formData = CsrfCookie.getFormData(data);

            $http({
                method: 'POST',
                url: '/app_dev.php/api/page/update/'+id,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };
        service.delete = function(id,  successCallback, errorCallback){

            var formData = CsrfCookie.getFormData([]);

            $http({
                method: 'POST',
                url: '/app_dev.php/api/page/delete/'+id,
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(successCallback,errorCallback);
        };

        return service;
    }]);