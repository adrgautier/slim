<?php

/**
 * Rendering the response for the User API
 */

use Slim\Http\Request;
use Slim\Http\Response;

$this->post('/read[/{id}[/]]', function(Request $request, Response $response, $args) {
    $response = $response->withHeader('Content-Type', 'application/json; charset=utf-8');
    if(isset($args['id'])){
        try{
            $data = $this->resource->user->read($args['id']);
        }
        catch(Exception $e){
            return $response->withStatus(400,utf8_decode($e->getMessage()));
        }
    }
    else{
        try{
            $data = $this->resource->user->readAll();
        }
        catch(Exception $e){
            return $response->withStatus(400,utf8_decode($e->getMessage()));
        }
    }
    echo json_encode($data);
    return $response->withHeader('Content-Type', 'application/json; charset=utf-8');
});

$this->post('/create[/]', function(Request $request, Response $response, $args) {
    try{
        $body = $request->getParsedBody();
        $files = $request->getUploadedFiles();
        echo json_encode($this->resource->user->create($body, $files));
    }
    catch(Exception $e){
        return $response->withStatus(400,utf8_decode($e->getMessage()));
    }
    return $response->withStatus(200,utf8_decode('Utilisateur ajouté.'));
});

$this->post('/update/{id}[/]', function(Request $request, Response $response, $args) {
    try{
        $body = $request->getParsedBody();
        $files = $request->getUploadedFiles();
        echo json_encode($this->resource->user->update($args['id'], $body, $files));
    }
    catch(Exception $e){
        return $response->withStatus(400,utf8_decode($e->getMessage()));
    }
    return $response->withStatus(200,utf8_decode('Utilisateur modifié.'));
});

$this->post('/delete/{id}[/]', function(Request $request, Response $response, $args) {
    try{
        echo json_encode($this->resource->user->delete($args['id']));
    }
    catch(Exception $e){
        return $response->withStatus(400,utf8_decode($e->getMessage()));
    }
    return $response->withStatus(200,utf8_decode('Utilisateur supprimé.'));
});