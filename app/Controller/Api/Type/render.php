<?php

use Slim\Http\Request;
use Slim\Http\Response;

$this->post('/read[/{id}[/]]', function(Request $request, Response $response, $args) {
    $response = $response->withHeader('Content-Type', 'application/json; charset=utf-8');
    if(isset($args['id'])){
        try{$data = $this->resource->type->read($args['id']);}
        catch(Exception $e){
            return $response->withStatus(400,$e->getMessage());
        }
    }
    else{
        try{$data = $this->resource->type->readAll();}
        catch(Exception $e){
            return $response->withStatus(400,$e->getMessage());
        }
    }
    echo json_encode($data);
    return $response->withHeader('Content-Type', 'application/json; charset=utf-8');
});