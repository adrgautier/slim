angular.module('TypeApi',['csrfModule'])
    .factory('TypeService',['$http', 'csrfCookieService', function($http,CsrfCookie) {

        var service = {};
            service.readAll = function(successCallback,errorCallback){
                var data = CsrfCookie.getTokens();

                var formData = new FormData();

                for(var field in data){
                    if(Array.isArray(data[field])){
                        formData.append(field,JSON.stringify(data[field]));
                    }
                    else{
                        formData.append(field,data[field]);
                    }
                }

                $http({
                    method: 'POST',
                    data: formData,
                    url: '/api/type/read',
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(successCallback,errorCallback);
            };

        return service;
    }]);