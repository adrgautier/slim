<?php

use Slim\Http\Request;
use Slim\Http\Response;

//  Contrainer
$container = $app->getContainer();


session_start();    //required CSRF protection starts on contruct whereas SessionMiddleware starts on invoke...

//CSRF protection
$container['csrf'] = function ($c) {
    $guard = new \Slim\Csrf\Guard();
    $guard->setFailureCallable(function (Request $request, Response $response, $next) {
        $response = $response->withStatus(401,'Failed CSRF check!');
        return $response;
    });
    return $guard;
};

//Session Class
$container['session'] = function ($c) {
    return new \RKA\Session();
};

//Resource
$container['resource'] = function($c) {
    return new app\Model\shared\ResourceContainer($c);
};

//TWIG template render
$container['view'] = function ($c) {
    $config = [];

    switch(MODE){
        case "prod":
            $config['cache'] = __APP__.'/../cache';
            break;
        case "dev":
            $config['debug'] = true;
            break;
    }

    $view = new \Slim\Views\Twig(__APP__.'/View',$config);

    //doesn't seem to work
    $view->addExtension(new \Slim\Views\TwigExtension(
        $c['router'],
        $c['request']->getUri()
    ));

    //environment
    $environment = $view->getEnvironment();
    $environment->addGlobal("helper", new \app\View\shared\Helper(MODE, $c));
    $environment->addGlobal("ui", new \app\View\shared\Ui());
    $header = [];

    $categories = $c->resource->category->readAll();

    $header["contents"] = $categories;

    //header
    $environment->addGlobal("header", $header);

    if(isset($c->session->user)){
        $environment->addGlobal("user", $c->session->user);
    }

    return $view;
};

//TWIG partial template render
$container['partialView'] = function ($c) {
    $config = [];

    switch (MODE) {
        case "prod":
            $config['cache'] = __APP__ . '/../cache';
            break;
        case "dev":
            $config['debug'] = true;
            break;
    }

    $view = new app\View\shared\Twig(__APP__ . '/View', $config);

    return $view;
};



$container['notFoundPage'] = function ($c) {
    return function ($message) use ($c) {
        $view = new \Slim\Views\Twig(__APP__.'/View');

        $resp = $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html; charset=utf-8');

        return $view->render($resp, 'error/404.html.twig', ['message' => $message]);
    };
};