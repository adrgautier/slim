<?php

namespace app\Model;

use app\Model\Entity\User;
use Slim\Container;
use app\shared\Doctrine;

abstract class AbstractResource
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager = null;
    /** @var Container  */
    private $container;

    /**
     * AbstractResource constructor.
     * @param Container $c
     */
    public function __construct(Container $c){

        $this->container = $c;
        $this->entityManager = Doctrine::getEntityManager($c->doctrine["connection"],true);
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->entityManager;
    }

    protected function getRequest(){
        return $this->container->request;
    }

    protected function getResource($resource){
        return $this->container->resource->$resource;
    }
    /** @return User */
    protected function getSessionUser(){
        return $this->entityManager->getRepository('app\Model\Entity\User')->find($this->container->session->user->getId());
    }
}