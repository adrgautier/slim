<?php

namespace app\Model;

use Exception;

abstract class AbstractEntity
{
    public function setField($body, $field, $required){
        $key = strtolower($field);

        if(isset($body[$key])){
            try{
                $this->{"set".$field}($body[$key]);
            }
            catch(Exception $e){
                throw $e;
            }
        }
        elseif($required){
            throw new Exception("The following field is required: $key");
        }
    }
}