<?php
namespace app\Model\shared;

use app\Model\Resource\Category;
use app\Model\Resource\Property;
use app\Model\Resource\User;
use app\Model\Resource\Type;
use app\Model\Resource\Content;
use app\Model\Resource\Page;
use app\Model\Resource\Bloc;
use app\Model\Resource\PageTemplate;
use app\Model\Resource\BlocTemplate;
use app\Model\Resource\InstanceRelation;

class ResourceContainer {

    public function __construct($c)
    {
        //$this->container = $c;
        $this->user = new User($c);
        $this->category = new Category($c);
        $this->type = new Type($c);
        $this->property = new Property($c);
        $this->content = new Content($c);

        $this->page = new Page($c);
        $this->bloc = new Bloc($c);

        $this->pageTemplate = new PageTemplate($c);
        $this->blocTemplate = new BlocTemplate($c);
        $this->relation = new InstanceRelation($c);
    }

    /*
    public function __invoke()
    {

    }
    */
}