<?php
namespace app\Model\shared;

use app\shared\Helper;

class SessionUser
{
    private $user;

    const USER = 1;
    const MODER = 2;
    const ADMIN = 3;
    const SUPER = 4;
    const NOON = 5;

    public function __construct($user)
    {
        $this->user = $user;

    }

    public function getId(){
        return $this->user["id"];
    }

    public function getName(){
        return $this->user["name"];
    }

    public function getMail(){
        return $this->user["mail"];
    }

    public function getRole(){
        return $this->user["role"];
    }

    public function getRoleWeight(){
        $userRole = $this->user["role"];
        return constant("self::$userRole");
    }

    public function hasAvatar(){
        if($this->user["avatar"] != null){
            return true;
        }
        else{
            return false;
        }
    }

    public function getAvatarExtension(){
        return $this->user["avatarExtension"];
    }

    public function hasResourcePermission($resource,$action = null){

        $userRole = $this->getRole();
        if($action){
            //find the permission for the given action
            $allowedRole  = Helper::loadConf("permissions")["resource"][$resource][$action];
        }
        else{
            //find the minimum role allowed
            $actions = Helper::loadConf("permissions")["resource"][$resource];
            $allowedRole = "SUPER";
            foreach($actions as $action=>$role){
                if(constant("self::$role")<constant("self::$allowedRole")){
                    $allowedRole = $role;
                }
            }
        }
        //todo: throw error if conf missing

        if(constant("self::$userRole")>=constant("self::$allowedRole")){
            return true;
        }
        else{
            //permission denied
            return false;
        }
    }
}