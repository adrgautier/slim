<?php

namespace app\Model\Entity;

use app\Model\AbstractEntity;
use app\Model\Entity;
use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="page")
 */
class Page extends AbstractEntity
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Column(name="url", type="string", length=128, nullable=false, unique=true)
     */
    private $url;

    /**
     * @var boolean
     * @Column(name="activation", type="boolean", nullable=false)
     */
    private $activation;

    /**
     * @var string
     * @Column(name="priority", type="decimal", precision=1, scale=1, nullable=false, options={"default":0.5})
     */
    private $priority;

    /**
     * @var string
     * @Column(name="frequency", type="string", columnDefinition="ENUM('always', 'hourly', 'daily', 'weekly', 'monthly', 'yearly', 'never')", nullable=false)
     */
    private $frequency;

    /**
     * @var int
     * @Column(name="position", type="smallint", nullable=false)
     */
    private $position;

    /**
     * @var string
     * @Column(name="title", type="string", length=128, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @Column(name="description", type="string", length=1024, nullable=true)
     */
    private $description;

    /**
     * @ManyToOne(targetEntity="Language")
     * @JoinColumn(name="language_id", referencedColumnName="id", nullable=false)
     */
    private $language;

    /**
     * @ManyToOne(targetEntity="Instance")
     * @JoinColumn(name="instance_id", referencedColumnName="id", nullable=false)
     */
    private $instance;

    /**
     * @ManyToOne(targetEntity="PageTemplate")
     * @JoinColumn(name="template_id", referencedColumnName="id", nullable=true)
     */
    private $template;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Page
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set activation
     *
     * @param boolean $activation
     * @return Page
     */
    public function setActivation($activation)
    {
        $this->activation = $activation;

        return $this;
    }

    /**
     * Get activation
     *
     * @return boolean 
     */
    public function getActivation()
    {
        return $this->activation;
    }

    /**
     * Set priority
     *
     * @param string $priority
     * @return Page
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return string 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set frequency
     *
     * @param string $frequency
     * @return Page
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return string 
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Page
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Page
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set language
     *
     * @param \app\Model\Entity\Language $language
     * @return Page
     */
    public function setLanguage(\app\Model\Entity\Language $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \app\Model\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set instance
     *
     * @param \app\Model\Entity\Instance $instance
     * @return Page
     */
    public function setInstance(\app\Model\Entity\Instance $instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * Get instance
     *
     * @return \app\Model\Entity\Instance 
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * Set template
     *
     * @param \app\Model\Entity\PageTemplate $template
     * @return Page
     */
    public function setTemplate(\app\Model\Entity\PageTemplate $template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return \app\Model\Entity\PageTemplate
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Converts the entity to an array.
     *
     * @return array
     */
    public function toArray(){
        return [
            'id' => $this->getId(),
            'url' => $this->getUrl(),
            'activation' => $this->getActivation(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'language' => $this->getLanguage()->getCode(),
            'template' => $this->getTemplate()->getName()
        ];
    }

}
