<?php

namespace app\Model\Entity;

use app\Model\AbstractEntity;
use Doctrine\ORM\Mapping;
use Psr\Http\Message\UploadedFileInterface;

/**
 * @Entity
 * @Table(name="user")
 */
class User extends AbstractEntity
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Column(name="mail", type="string", length=128, nullable=false, unique=true)
     */
    private $mail;

    /**
     * @var string
     * @Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var resource
     * @Column(name="password", type="binary", length=32, nullable=false)
     */
    private $password;

    /**
     * @var string
     * @Column(name="role", type="string", columnDefinition="ENUM('USER', 'MODER', 'ADMIN', 'SUPER')", nullable=false)
     */
    private $role = 'USER';

    /**
     * @var string
     * @Column(name="avatar", type="text", nullable=true)
     */
    private $avatar;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return User
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     * @throws \Exception
     */
    public function setPassword($password)
    {
        $password = json_decode($password, true);
        $newPassword = null;
        if($password["new"] == $password["repeat"]) {
            if($currentPassword = $this->getPassword()){
                if(hash('sha256',$password["current"]) == $currentPassword){
                    $newPassword  = $password["new"];
                }
                else{
                    throw new \Exception("The current password is not valid.");
                }
            }
            else {
                $newPassword  = $password["new"];
            }
        }
        else{
            throw new \Exception("The new passwords are not the same.");
        }

        if($newPassword != null){
            $stream = fopen('php://memory','r+');
            fwrite($stream, hex2bin(hash('sha256',$newPassword)));
            rewind($stream);

            $this->password = $stream;
        }

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        if($this->password){
            return bin2hex(stream_get_contents($this->password));
        }
        else{
            return null;
        }

    }

    /**
     * Set role
     *
     * @param string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set avatar
     *
     * @param UploadedFileInterface $avatar
     * @return User
     */
    public function setAvatar(UploadedFileInterface $avatar)
    {
        $ctx = hash_init('md5');
        hash_update_stream($ctx, $avatar->getStream()->detach());
        $hash = hash_final($ctx);

        $file = pathinfo($avatar->getClientFileName(), PATHINFO_EXTENSION).";".$hash;

        $this->avatar = $file;

        return $this;
    }

    /**
     * Get avatar
     *
     * @param string $mode
     * @return string
     */
    public function getAvatar($mode = null)
    {
        switch($mode){
            case "extension":
                $extension = explode(";",$this->avatar)[0];
                return $extension !== "" ? $extension : null;
                break;
            case "hash":
                $hash = null;
                if($this->avatar){
                    $hash = explode(";",$this->avatar)[1];
                }
                return $hash;
                break;
            default:
                return $this->avatar;
                break;
        }
    }

    /**
     * Converts the entity to an array.
     *
     * @return array
     */
    public function toArray(){
        return [
            'id' => $this->getId(),
            'mail' => $this->getMail(),
            'name' => $this->getName(),
            'password' => $this->getPassword(),
            'role' => $this->getRole(),
            'avatar' => $this->getAvatar(),
            'avatarHash' => $this->getAvatar("hash"),
            'avatarExtension' => $this->getAvatar("extension")
        ];
    }
}
