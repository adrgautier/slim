<?php

namespace app\Model\Entity;

use DateTime;
use app\Model\Entity;
use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="value")
 */
class Value {
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Instance", inversedBy="values", cascade={"persist"})
     * @JoinColumn(name="instance_id", referencedColumnName="id", nullable=false)
     */
    private $instance;

    /**
     * @ManyToOne(targetEntity="Property", inversedBy="values", cascade={"persist"})
     * @JoinColumn(name="property_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $property;

    /**
     * @ManyToOne(targetEntity="Language", cascade={"persist"})
     * @JoinColumn(name="language_id", referencedColumnName="id", nullable=false)
     */
    private $language;

    /**
     * @var int
     * @Column(name="position", type="smallint", nullable=true)
     */
    private $position;

    /**
     * @var string
     * @Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var DateTime
     * @Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @ManyToOne(targetEntity="User", cascade={"persist"})
     * @JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=false)
     */
    private $creationUser;

    /**
     * @var DateTime
     * @Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @ManyToOne(targetEntity="User", cascade={"persist"})
     * @JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     */
    private $updateUser;

    /**
     * @var integer
     * @Column(name="update_version", type="smallint", nullable=true)
     */
    private $updateVersion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Value
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Value
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Value
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Value
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set updateVersion
     *
     * @param integer $updateVersion
     * @return Value
     */
    public function setUpdateVersion($updateVersion)
    {
        $this->updateVersion = $updateVersion;

        return $this;
    }

    /**
     * Get updateVersion
     *
     * @return integer 
     */
    public function getUpdateVersion()
    {
        return $this->updateVersion;
    }

    /**
     * Set instance
     *
     * @param \app\Model\Entity\Instance $instance
     * @return Value
     */
    public function setInstance(\app\Model\Entity\Instance $instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * Get instance
     *
     * @return \app\Model\Entity\Instance 
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * Set property
     *
     * @param \app\Model\Entity\Property $property
     * @return Value
     */
    public function setProperty(\app\Model\Entity\Property $property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \app\Model\Entity\Property 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set language
     *
     * @param \app\Model\Entity\Language $language
     * @return Value
     */
    public function setLanguage(\app\Model\Entity\Language $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \app\Model\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set creationUser
     *
     * @param \app\Model\Entity\User $creationUser
     * @return Value
     */
    public function setCreationUser(\app\Model\Entity\User $creationUser)
    {
        $this->creationUser = $creationUser;

        return $this;
    }

    /**
     * Get creationUser
     *
     * @return \app\Model\Entity\User 
     */
    public function getCreationUser()
    {
        return $this->creationUser;
    }

    /**
     * Set updateUser
     *
     * @param \app\Model\Entity\User $updateUser
     * @return Value
     */
    public function setUpdateUser(\app\Model\Entity\User $updateUser = null)
    {
        $this->updateUser = $updateUser;

        return $this;
    }

    /**
     * Get updateUser
     *
     * @return \app\Model\Entity\User 
     */
    public function getUpdateUser()
    {
        return $this->updateUser;
    }
}
