<?php

namespace app\Model\Entity;

use app\Model\AbstractEntity;
use DateTime;
use app\Model\Entity;
use Doctrine\ORM\Mapping;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="instance")
 */
class Instance extends AbstractEntity{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Column(name="name", type="string", length=64, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var boolean
     * @Column(name="activation", type="boolean", nullable=false)
     */
    private $activation;

    /**
     * @var DateTime
     * @Column(name="activation_date", type="datetime", nullable=true)
     */
    private $activationDate;


    /**
     * @var int
     * @Column(name="position", type="smallint", nullable=false)
     */
    private $position;

    /**
     * @var DateTime
     * @Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=false)
     */
    private $creationUser;

    /**
     * @var DateTime
     * @Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     */
    private $updateUser;

    /**
     * @var integer
     * @Column(name="update_version", type="smallint", nullable=true)
     */
    private $updateVersion;

    /**
     * @ManyToOne(targetEntity="Category", inversedBy="instances")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    private $category;

    /**
     * @OneToMany(targetEntity="Value", mappedBy="instance", cascade={"remove"})
     */
    private $values;

    /**
     * @OneToMany(targetEntity="InstanceRelation", mappedBy="parent")
     */
    private $relationsAsParent;

    /**
     * @OneToMany(targetEntity="InstanceRelation", mappedBy="child", cascade={"remove"})
     */
    private $relationsAsChild;

    /**
     * @OneToMany(targetEntity="Entry", mappedBy="instance")
     */
    private $entries;

    public function __construct() {
        $this->values = new ArrayCollection();
        $this->relationsAsChild = new ArrayCollection();
        $this->relationsAsParent = new ArrayCollection();
        $this->entries = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Instance
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set activation
     *
     * @param boolean $activation
     * @return Instance
     */
    public function setActivation($activation)
    {
        $this->activation = $activation;

        return $this;
    }

    /**
     * Get activation
     *
     * @return boolean 
     */
    public function getActivation()
    {
        return $this->activation;
    }

    /**
     * Set activationDate
     *
     * @param \DateTime $activationDate
     * @return Instance
     */
    public function setActivationDate($activationDate)
    {
        $this->activationDate = $activationDate;

        return $this;
    }

    /**
     * Get activationDate
     *
     * @return \DateTime 
     */
    public function getActivationDate()
    {
        return $this->activationDate;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Instance
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Instance
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Instance
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set updateVersion
     *
     * @param integer $updateVersion
     * @return Instance
     */
    public function setUpdateVersion($updateVersion)
    {
        $this->updateVersion = $updateVersion;

        return $this;
    }

    /**
     * Get updateVersion
     *
     * @return integer 
     */
    public function getUpdateVersion()
    {
        return $this->updateVersion;
    }

    /**
     * Set creationUser
     *
     * @param \app\Model\Entity\User $creationUser
     * @return Instance
     */
    public function setCreationUser(\app\Model\Entity\User $creationUser)
    {
        $this->creationUser = $creationUser;

        return $this;
    }

    /**
     * Get creationUser
     *
     * @return \app\Model\Entity\User 
     */
    public function getCreationUser()
    {
        return $this->creationUser;
    }

    /**
     * Set updateUser
     *
     * @param \app\Model\Entity\User $updateUser
     * @return Instance
     */
    public function setUpdateUser(\app\Model\Entity\User $updateUser = null)
    {
        $this->updateUser = $updateUser;

        return $this;
    }

    /**
     * Get updateUser
     *
     * @return \app\Model\Entity\User 
     */
    public function getUpdateUser()
    {
        return $this->updateUser;
    }

    /**
     * Set category
     *
     * @param \app\Model\Entity\Category $category
     * @return Instance
     */
    public function setCategory(\app\Model\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \app\Model\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add values
     *
     * @param \app\Model\Entity\Value $values
     * @return Instance
     */
    public function addValue(\app\Model\Entity\Value $values)
    {
        $this->values[] = $values;

        return $this;
    }

    /**
     * Remove values
     *
     * @param \app\Model\Entity\Value $values
     */
    public function removeValue(\app\Model\Entity\Value $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Add relationsAsParent
     *
     * @param \app\Model\Entity\InstanceRelation $relationsAsParent
     * @return Instance
     */
    public function addRelationsAsParent(\app\Model\Entity\InstanceRelation $relationsAsParent)
    {
        $this->relationsAsParent[] = $relationsAsParent;

        return $this;
    }

    /**
     * Remove relationsAsParent
     *
     * @param \app\Model\Entity\InstanceRelation $relationsAsParent
     */
    public function removeRelationsAsParent(\app\Model\Entity\InstanceRelation $relationsAsParent)
    {
        $this->relationsAsParent->removeElement($relationsAsParent);
    }

    /**
     * Get relationsAsParent
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelationsAsParent()
    {
        return $this->relationsAsParent;
    }

    /**
     * Add relationsAsChild
     *
     * @param \app\Model\Entity\InstanceRelation $relationsAsChild
     * @return Instance
     */
    public function addRelationsAsChild(\app\Model\Entity\InstanceRelation $relationsAsChild)
    {
        $this->relationsAsChild[] = $relationsAsChild;

        return $this;
    }

    /**
     * Remove relationsAsChild
     *
     * @param \app\Model\Entity\InstanceRelation $relationsAsChild
     */
    public function removeRelationsAsChild(\app\Model\Entity\InstanceRelation $relationsAsChild)
    {
        $this->relationsAsChild->removeElement($relationsAsChild);
    }

    /**
     * Get relationsAsChild
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelationsAsChild()
    {
        return $this->relationsAsChild;
    }

    /**
     * Add entries
     *
     * @param \app\Model\Entity\Entry $entries
     * @return Instance
     */
    public function addEntry(\app\Model\Entity\Entry $entries)
    {
        $this->entries[] = $entries;

        return $this;
    }

    /**
     * Remove entries
     *
     * @param \app\Model\Entity\Entry $entries
     */
    public function removeEntry(\app\Model\Entity\Entry $entries)
    {
        $this->entries->removeElement($entries);
    }

    /**
     * Get entries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * Converts the entity to an array.
     *
     * @return array
     */
    public function toArray(){
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'activation' => $this->getActivation(),
            'position' => $this->getPosition()
        ];
    }
}
