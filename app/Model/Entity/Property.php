<?php

namespace app\Model\Entity;

use app\Model\Entity;
use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="property")
 */
class Property {
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     * @Column(name="activation", type="boolean", nullable=false)
     */
    private $activation;

    /**
     * @var int
     * @Column(name="position", type="smallint", nullable=false)
     */
    private $position;

    /**
     * @var string
     * @Column(name="priority", type="decimal", precision=1, scale=1, nullable=true)
     */
    private $priority;

    /**
     * @ManyToOne(targetEntity="Category", cascade={"persist"})
     * @JoinColumn(name="parent_category_id", referencedColumnName="id", nullable=true)
     */
    private $parentCategory;

    /**
     * @ManyToOne(targetEntity="Instance", cascade={"persist"})
     * @JoinColumn(name="parent_instance_id", referencedColumnName="id", nullable=true)
     */
    private $parentInstance;

    /**
     * @ManyToOne(targetEntity="Value")
     * @JoinColumn(name="default_value_id", referencedColumnName="id", nullable=true)
     */
    private $defaultValue;

    /**
     * @OneToMany(targetEntity="Value", mappedBy="property", cascade={"persist","remove"})
     */
    private $values;

    /**
     * @ManyToOne(targetEntity="Type", cascade={"persist"})
     * @JoinColumn(name="type_id", referencedColumnName="id", nullable=true)
     */
    private $type;

    /**
     * @ManyToOne(targetEntity="Category", cascade={"persist"})
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    private $category;

    /**
     * @var boolean
     * @Column(name="multiple", type="boolean", nullable=true)
     */
    private $multiple;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Property
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set activation
     *
     * @param boolean $activation
     * @return Property
     */
    public function setActivation($activation)
    {
        $this->activation = $activation;

        return $this;
    }

    /**
     * Get activation
     *
     * @return boolean 
     */
    public function getActivation()
    {
        return $this->activation;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Property
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set priority
     *
     * @param string $priority
     * @return Property
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return string 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set multiple
     *
     * @param boolean $multiple
     * @return Property
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return boolean 
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * Set parentCategory
     *
     * @param \app\Model\Entity\Category $parentCategory
     * @return Property
     */
    public function setParentCategory(\app\Model\Entity\Category $parentCategory = null)
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    /**
     * Get parentCategory
     *
     * @return \app\Model\Entity\Category 
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }

    /**
     * Set parentInstance
     *
     * @param \app\Model\Entity\Instance $parentInstance
     * @return Property
     */
    public function setParentInstance(\app\Model\Entity\Instance $parentInstance = null)
    {
        $this->parentInstance = $parentInstance;

        return $this;
    }

    /**
     * Get parentInstance
     *
     * @return \app\Model\Entity\Instance 
     */
    public function getParentInstance()
    {
        return $this->parentInstance;
    }

    /**
     * Set defaultValue
     *
     * @param \app\Model\Entity\Value $defaultValue
     * @return Property
     */
    public function setDefaultValue(\app\Model\Entity\Value $defaultValue = null)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return \app\Model\Entity\Value 
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Add values
     *
     * @param \app\Model\Entity\Value $values
     * @return Property
     */
    public function addValue(\app\Model\Entity\Value $values)
    {
        $this->values[] = $values;

        return $this;
    }

    /**
     * Remove values
     *
     * @param \app\Model\Entity\Value $values
     */
    public function removeValue(\app\Model\Entity\Value $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set type
     *
     * @param \app\Model\Entity\Type $type
     * @return Property
     */
    public function setType(\app\Model\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \app\Model\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set category
     *
     * @param \app\Model\Entity\Category $category
     * @return Property
     */
    public function setCategory(\app\Model\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \app\Model\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Converts the entity to an array.
     *
     * @return array
     */
    public function toArray(){
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "activation" => $this->getActivation(),
            "position" => $this->getPosition(),
            "parentCategory" => $this->getParentCategory(),
            "parentInstance" => $this->getParentInstance(),
            "type" => $this->getType(),
            "category" => $this->getCategory()
        ];
    }
}
