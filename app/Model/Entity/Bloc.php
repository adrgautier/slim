<?php

namespace app\Model\Entity;

use app\Model\AbstractEntity;
use app\Model\Entity;
use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="bloc")
 */
class Bloc extends AbstractEntity
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Column(name="anchor", type="string", length=64, nullable=false, unique=true)
     */
    private $anchor;

    /**
     * @var boolean
     * @Column(name="activation", type="boolean", nullable=false)
     */
    private $activation;

    /**
     * @var int
     * @Column(name="position", type="smallint", nullable=false)
     */
    private $position;

    /**
     * @ManyToOne(targetEntity="Language")
     * @JoinColumn(name="language_id", referencedColumnName="id", nullable=false)
     */
    private $language;

    /**
     * @OneToOne(targetEntity="InstanceRelation", inversedBy="bloc")
     * @JoinColumn(name="instance_instance_id", referencedColumnName="id", nullable=false)
     */
    private $instanceRelation;

    /**
     * @ManyToOne(targetEntity="BlocTemplate")
     * @JoinColumn(name="template_id", referencedColumnName="id", nullable=true)
     */
    private $template;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set anchor
     *
     * @param string $anchor
     * @return Bloc
     */
    public function setAnchor($anchor)
    {
        $this->anchor = $anchor;

        return $this;
    }

    /**
     * Get anchor
     *
     * @return string 
     */
    public function getAnchor()
    {
        return $this->anchor;
    }

    /**
     * Set activation
     *
     * @param boolean $activation
     * @return Bloc
     */
    public function setActivation($activation)
    {
        $this->activation = $activation;

        return $this;
    }

    /**
     * Get activation
     *
     * @return boolean 
     */
    public function getActivation()
    {
        return $this->activation;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Bloc
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set language
     *
     * @param \app\Model\Entity\Language $language
     * @return Bloc
     */
    public function setLanguage(\app\Model\Entity\Language $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \app\Model\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set instanceRelation
     *
     * @param \app\Model\Entity\InstanceRelation $instanceRelation
     * @return Bloc
     */
    public function setInstanceRelation(\app\Model\Entity\InstanceRelation $instanceRelation)
    {
        $this->instanceRelation = $instanceRelation;

        return $this;
    }

    /**
     * Get instanceRelation
     *
     * @return \app\Model\Entity\InstanceRelation
     */
    public function getInstanceRelation()
    {
        return $this->instanceRelation;
    }

    /**
     * Set template
     *
     * @param \app\Model\Entity\BlocTemplate $template
     * @return Bloc
     */
    public function setTemplate(\app\Model\Entity\BlocTemplate $template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return \app\Model\Entity\BlocTemplate
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Converts the entity to an array.
     *
     * @return array
     */
    public function toArray(){
        return [
            'id' => $this->getId(),
            'anchor' => $this->getAnchor(),
            'activation' => $this->getActivation(),
            'position' => $this->getPosition(),
            'language' => $this->getLanguage()->getCode(),
            'template' => $this->getTemplate()->getName()
        ];
    }

}
