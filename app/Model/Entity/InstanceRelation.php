<?php

namespace app\Model\Entity;

use app\Model\Entity;
use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="instance_instance")
 */
class InstanceRelation {
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Instance", inversedBy="relationsAsParent")
     * @JoinColumn(name="parent_id", referencedColumnName="id", nullable=false)
     */
    private $parent;

    /**
     * @ManyToOne(targetEntity="Instance", inversedBy="relationsAsChild")
     * @JoinColumn(name="child_id", referencedColumnName="id", nullable=false)
     */
    private $child;

    /**
     * @ManyToOne(targetEntity="Property")
     * @JoinColumn(name="property_id", referencedColumnName="id", nullable=false)
     */
    private $property;

    /**
     * @OneToOne(targetEntity="Bloc", mappedBy="instanceRelation", cascade={"remove"})
     */
    private $bloc;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parent
     *
     * @param \app\Model\Entity\Instance $parent
     * @return InstanceRelation
     */
    public function setParent(\app\Model\Entity\Instance $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \app\Model\Entity\Instance 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set child
     *
     * @param \app\Model\Entity\Instance $child
     * @return InstanceRelation
     */
    public function setChild(\app\Model\Entity\Instance $child)
    {
        $this->child = $child;

        return $this;
    }

    /**
     * Get child
     *
     * @return \app\Model\Entity\Instance 
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set property
     *
     * @param \app\Model\Entity\Property $property
     * @return InstanceRelation
     */
    public function setProperty(\app\Model\Entity\Property $property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \app\Model\Entity\Property 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set bloc
     *
     * @param \app\Model\Entity\Bloc $bloc
     * @return InstanceRelation
     */
    public function setBloc(\app\Model\Entity\Bloc $bloc = null)
    {
        $this->bloc = $bloc;

        return $this;
    }

    /**
     * Get bloc
     *
     * @return \app\Model\Entity\Bloc 
     */
    public function getBloc()
    {
        return $this->bloc;
    }
}
