<?php

namespace app\Model\Entity;

use app\Model\Entity;
use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="entry")
 */
class Entry {
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Instance", inversedBy="entries")
     * @JoinColumn(name="instance_id", referencedColumnName="id", nullable=false)
     */
    private $instance;

    /**
     * @ManyToOne(targetEntity="Header", inversedBy="entries")
     * @JoinColumn(name="header_id", referencedColumnName="id", nullable=true)
     */
    private $header;

    /**
     * @ManyToOne(targetEntity="Footer", inversedBy="entries")
     * @JoinColumn(name="footer_id", referencedColumnName="id", nullable=true)
     */
    private $footer;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set instance
     *
     * @param \app\Model\Entity\Instance $instance
     * @return Entry
     */
    public function setInstance(\app\Model\Entity\Instance $instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * Get instance
     *
     * @return \app\Model\Entity\Instance 
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * Set header
     *
     * @param \app\Model\Entity\Header $header
     * @return Entry
     */
    public function setHeader(\app\Model\Entity\Header $header = null)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return \app\Model\Entity\Header 
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set footer
     *
     * @param \app\Model\Entity\Footer $footer
     * @return Entry
     */
    public function setFooter(\app\Model\Entity\Footer $footer = null)
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * Get footer
     *
     * @return \app\Model\Entity\Footer 
     */
    public function getFooter()
    {
        return $this->footer;
    }
}
