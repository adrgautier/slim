<?php

namespace app\Model\Entity;

use app\Model\Entity;
use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="type")
 */
class Type
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Column(name="name", type="string", length=64, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var boolean
     * @Column(name="activation", type="boolean", nullable=false)
     */
    private $activation;

    /**
     * @var int
     * @Column(name="position", type="smallint", nullable=false)
     */
    private $position;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Type
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set activation
     *
     * @param boolean $activation
     * @return Type
     */
    public function setActivation($activation)
    {
        $this->activation = $activation;

        return $this;
    }

    /**
     * Get activation
     *
     * @return boolean 
     */
    public function getActivation()
    {
        return $this->activation;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Type
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Converts the entity to an array.
     *
     * @return array
     */
    public function toArray(){
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'activation' => $this->getActivation(),
            'position' => $this->getPosition()
        ];
    }
}
