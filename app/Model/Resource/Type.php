<?php

namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\Type as TypeEntity;
use Doctrine\ORM\EntityManager;
use Exception;

/**
 * Class Type Resource
 */
class Type extends AbstractResource
{

    /**
     * Reads the type of the given id.
     *
     * @param $id
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function read($id)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('t')
            ->from('app\Model\Entity\Type', 't')
            ->where("t.id = '$id'")
            ->orderBy('t.position', 'ASC')
            ->getQuery();

        /** @var TypeEntity $type */
        $type = $query->getOneOrNullResult();

        if($type != null) {
            return $type->toArray();
        }
        else {
            throw new Exception("Type not found", 404);
        }
    }

    /**
     * Reads all the existing types.
     *
     * @return array
     */
    public function readAll(){
        /** @var EntityManager $em */
        $em = self::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('t')
            ->from('app\Model\Entity\Type', 't')
            ->orderBy('t.position', 'ASC')
            ->getQuery();

        $types = $query->getResult();

        $array = array_map(function (TypeEntity $type) {
            return $type->toArray();
        }, $types);

        return $array;
    }
}