<?php

namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\Property as PropertyEntity;
use Doctrine\ORM\EntityManager;
use Exception;

/**
 * Class Property Resource
 */
class Property extends AbstractResource
{
    /**
     * Reads the property of the given id.
     *
     * @param  int $id
     * @throws Exception if the property is not found.
     * @return array
     */
    public function read($id)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
            ->from('app\Model\Entity\Property', 'p')
            ->where("p.id = '$id'")
            ->orderBy('p.position', 'ASC')
            ->getQuery();

        /** @var PropertyEntity $property */
        $property = $query->getOneOrNullResult();

        if($property != null) {
            return $property->toArray();
        }
        else {
            throw new Exception("Property not found.", 404);
        }
    }

}