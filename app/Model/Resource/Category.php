<?php

namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\Category as CategoryEntity;

include("Category/_read.php");
include("Category/_update.php");

/**
 * Class Category Resource
 */
class Category extends AbstractResource
{

    use \readCategory;
    use \updateCategory;

    const ENTITY = 'app\Model\Entity\Category';
}