<?php

namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\Bloc as BlocEntity;
use app\Model\Entity\Instance;
use Doctrine\ORM\EntityManager;
use Exception;
use app\Model\Entity\InstanceRelation;

include("Bloc/_create.php");
include("Bloc/_read.php");
include("Bloc/_update.php");
include("Bloc/_delete.php");

/**
 * Class Bloc Resource
 */
class Bloc extends AbstractResource
{
    use \createBloc;
    use \readBloc;
    use \updateBloc;
    use \deleteBloc;
}