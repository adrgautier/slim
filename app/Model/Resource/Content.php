<?php

namespace app\Model\Resource;

use app\Model\AbstractResource;

include("Content/_create.php");
include("Content/_read.php");
include("Content/_update.php");
include("Content/_delete.php");


/**
 * Class Content Resource
 */
class Content extends AbstractResource
{
    use \createContent;
    use \readContent;
    use \updateContent;
    use \deleteContent;
}