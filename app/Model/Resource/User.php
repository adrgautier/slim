<?php

namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\User as UserEntity;

include("User/_create.php");
include("User/_read.php");
include("User/_update.php");
include("User/_delete.php");

/**
 * Class User Resource
 */
class User extends AbstractResource
{

    use \createUser;
    use \readUser;
    use \updateUser;
    use \deleteUser;

    const ENTITY = 'app\Model\Entity\User';
}