<?php
namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\BlocTemplate as BlocTemplateEntity;
use Doctrine\ORM\EntityManager;
use Exception;

/**
 * Class BlocTemplate Resource
 */
class BlocTemplate extends AbstractResource
{

    /**
     * Reads as select field
     *
     * @return array
     */
    public function readAllAsField(){
        /** @var EntityManager $em */
        $em = self::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('b')
            ->from('app\Model\Entity\BlocTemplate', 'b')
            ->orderBy('b.position', 'ASC')
            ->getQuery();

        $templates = $query->getResult();

        $field = ["type"=>"select","options"=>[]];

        /** @var BlocTemplateEntity $template */
        foreach($templates as $template){
            $option = ["name" => $template->getName(), "value" => $template->getId()];
            array_push($field["options"], $option);
        }

        return $field;
    }
}