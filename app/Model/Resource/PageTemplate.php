<?php
namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\PageTemplate as PageTemplateEntity;
use Doctrine\ORM\EntityManager;
use Exception;

/**
 * Class PageTemplate Resource
 */
class PageTemplate extends AbstractResource
{

    /**
     * Reads as select field
     *
     * @return array
     */
    public function readAllAsField(){
        /** @var EntityManager $em */
        $em = self::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
            ->from('app\Model\Entity\PageTemplate', 'p')
            ->orderBy('p.position', 'ASC')
            ->getQuery();

        $templates = $query->getResult();

        $field = ["type"=>"select","options"=>[]];

        /** @var PageTemplateEntity $template */
        foreach($templates as $template){
            $option = ["name" => $template->getName(), "value" => $template->getId()];
            array_push($field["options"], $option);
        }

        return $field;
    }
}