<?php

namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\Page as PageEntity;
use Doctrine\ORM\EntityManager;
use Exception;

include("Page/_create.php");
include("Page/_read.php");
include("Page/_update.php");
include("Page/_delete.php");

/**
 * Class Page Resource
 */
class Page extends AbstractResource
{
    use \createPage;
    use \readPage;
    use \updatePage;
    use \deletePage;
}