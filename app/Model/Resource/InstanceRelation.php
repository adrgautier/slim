<?php
namespace app\Model\Resource;

use app\Model\AbstractResource;
use app\Model\Entity\InstanceRelation as InstanceRelationEntity;
use Doctrine\ORM\EntityManager;
use Exception;

/**
 * Class InstanceRelation Resource
 */
class InstanceRelation extends AbstractResource
{

    /**
     * Reads as select field
     *
     * @return array
     */
    public function readAllAsField(){
        /** @var EntityManager $em */
        $em = self::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('i')
            ->from('app\Model\Entity\InstanceRelation', 'i')
            ->leftJoin('app\Model\Entity\Bloc','b','b.instanceRelation = i.id')
            ->where('b is null')
            ->getQuery();

        $instanceRelations = $query->getResult();

        $field = ["type"=>"select","options"=>[]];

        /** @var InstanceRelationEntity $instanceRelation */
        foreach($instanceRelations as $instanceRelation){
            $option = ["name" => $instanceRelation->getChild()->getName().' enfant de '.$instanceRelation->getParent()->getName(), "value" => $instanceRelation->getId()];
            array_push($field["options"], $option);
        }

        return $field;
    }
}