<?php

use app\Model\Entity\Page;

trait readPage{

    /**
     * Reads the page of the given id.
     *
     * @param $id
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function read($id)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
            ->from('app\Model\Entity\Page', 'p')
            ->where("p.id = '$id'")
            ->getQuery();

        /** @var Page $page */
        $page = $query->getOneOrNullResult();

        if($page != null) {
            return $page->toArray();
        }
        else {
            throw new Exception("Page not found", 404);
        }
    }

    /**
     * Reads the page of the given url.
     *
     * @param string $url
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function readFromUrl($url)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
            ->from('app\Model\Entity\Page', 'p')
            ->where("p.url = '$url'")
            ->getQuery();

        /** @var Page $page */
        $page = $query->getOneOrNullResult();


        if($page != null) {
            return $page->toArray();
        }
        else {
            throw new Exception("Page not found", 404);
        }
    }


    /**
     * Reads all the existing pages.
     *
     * @return array
     */
    public function readAll(){
        /** @var EntityManager $em */
        $em = self::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
            ->from('app\Model\Entity\Page', 'p')
            ->orderBy('p.position', 'ASC')
            ->getQuery();

        $pages = $query->getResult();

        $array = array_map(function (Page $page) {
            return $page->toArray();
        }, $pages);

        return $array;
    }

}