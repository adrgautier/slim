<?php

use app\Model\Entity\Page;

trait createPage{

    /**
     * Create page.
     *
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function create($body, $files)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $issues = [];

        /** @var Page $page */
        $page = new Page();

        //required fields
        foreach(["Url","Title","Description","Activation"] as $field){
            try {
                $page->setField($body, $field, true);
            }
            catch (Exception $e) {
                array_push($issues, $e->getMessage());
            }
        }
        try {
            $template = $em->getRepository('app\Model\Entity\PageTemplate')->find($body['template']);
            $page->setTemplate($template);
            $instance = $em->getRepository('app\Model\Entity\Instance')->find($body['content']);
            $page->setInstance($instance);
            $page->setPosition(0);
            $page->setPriority(0);
            $page->setFrequency('never');
            $page->setLanguage($em->getRepository('app\Model\Entity\Language')->find(1));
        }
        catch (Exception $e) {
            array_push($issues, $e->getMessage());
        }

        if(!count($issues)) {

            $em->persist($page);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }

        return $page->toArray();
    }
}