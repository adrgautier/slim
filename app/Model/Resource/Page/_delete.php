<?php

trait deletePage{

    /**
     * Deletes the page for the given id.
     *
     * @param $id
     * @return array
     * @throws Exception
     */
    public function delete($id){
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $qb = $em->createQueryBuilder();
        $query = $qb->select('u')
            ->from('app\Model\Entity\User', 'u')
            ->where("u.id = '$id'")
            ->getQuery();

        $page = $query->getOneOrNullResult();

        if($page != null) {
            //remove
            $em->remove($page);
            $em->flush();

            return $page->toArray();
        }
        else {
            throw new Exception("User not found", 404);
        }

    }

}