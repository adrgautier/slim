<?php

trait updatePage{

    /**
     * Updates the Page for the given id.
     *
     * @param int $id
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function update($id, $body, $files)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $issues = [];

        $qb = $em->createQueryBuilder();
        $query = $qb->select('u')
            ->from('app\Model\Entity\Page', 'u')
            ->where("u.id = '$id'")
            ->getQuery();

        /** @var \app\Model\Entity\Page $page */
        $page = $query->getOneOrNullResult();

        if($page == null) {
            throw new Exception("not found", 404);
        }

        //required fields
        foreach(["Url","Title","Description","Activation"] as $field){
            try {
                $page->setField($body, $field, true);
            }
            catch (Exception $e) {
                array_push($issues, $e->getMessage());
            }
        }
        try {
            $template = $em->getRepository('app\Model\Entity\PageTemplate')->find($body['template']);
            $page->setTemplate($template);
            $instance = $em->getRepository('app\Model\Entity\Instance')->find($body['content']);
            $page->setInstance($instance);
        }
        catch (Exception $e) {
            array_push($issues, $e->getMessage());
        }


        if(!count($issues)) {


            $em->persist($page);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }

        return $page->toArray();
    }
}