<?php

use Doctrine\ORM\EntityManager;

use app\Model\Entity\Category;
use app\Model\Entity\Property;
use app\Model\Entity\Instance;
use app\Model\Entity\InstanceRelation;
use app\Model\Entity\Value;
use app\Model\Entity\Language;

trait deleteContent{
    /**
     * Deletes the Content for the given id.
     *
     * @param int $id
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function delete($id, $body, $files)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $qb = $em->createQueryBuilder();
        $query = $qb->select('i')
            ->from('app\Model\Entity\Instance', 'i')
            ->where("i.id = '$id'")
            ->getQuery();

        /** @var \app\Model\Entity\Instance $instance */
        $instance = $query->getOneOrNullResult();

        if($instance == null) {
            throw new Exception("Instance not found or already deteted.", 404);
        }
        else{
            $em->remove($instance);
            $em->flush();
        }

        return $instance->toArray();
    }
}