<?php

use Doctrine\ORM\EntityManager;

use app\Model\Entity\Category;
use app\Model\Entity\Property;
use app\Model\Entity\Instance;
use app\Model\Entity\InstanceRelation;
use app\Model\Entity\Value;
use app\Model\Entity\Language;
use app\Model\Entity\Page;
use app\Model\Entity\Bloc;

trait readContent{
    /**
     * Reads the content of the given id.
     *
     * @param $id
     * @throws Exception
     * @return array
     */
    public function read($id)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('i')
            ->from('app\Model\Entity\Instance', 'i')
            ->where("i.id = '$id'")
            ->getQuery();

        /** @var Instance $instance */
        $instance = $query->getOneOrNullResult();

        if($instance != null) {
            $values = $instance->getValues();
            $valuesArray = [];
            /** @var Value $value */
            foreach($values as $value){
                $valuesArray[$value->getProperty()->getName()] = $value->getContent();
            }
            $relations = $instance->getRelationsAsChild();
            $relationsArray = [];
            /** @var InstanceRelation $relation */
            foreach($relations as $relation){
                $relationsArray[$relation->getProperty()->getName()] = $relation->getParent()->getId();
            }
            return array_merge($instance->toArray(),$valuesArray,$relationsArray);
        }
        else {
            throw new Exception("Instance not found.", 404);
        }
    }

    /**
     * Reads all the existing contents for the given category.
     *
     * @param string $category
     * @return array
     */
    public function readAll($category){
        /** @var EntityManager $em */
        $em = parent::getEntityManager();

        /** @var Category $category */
        $category = $em->getRepository('app\Model\Entity\Category')->findOneByName($category);

        if($category !== null){
            $qb = $em->createQueryBuilder();

            $query = $qb->select('i')
                ->from('app\Model\Entity\Instance', 'i')
                ->where("i.category = '".$category->getId()."'")
                ->orderBy('i.position', 'ASC')
                ->getQuery();

            $instances = $query->getResult();

            $array = array_map(function (Instance $instance) {
                return $instance->toArray();
            },
                $instances);

            return $array;
        }
    }

    /**
     * Reads the content associated to the given page id.
     *
     * @param int $pageId
     * @throws Exception
     * @return array
     */
    public function readFromPageId($pageId)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
            ->from('app\Model\Entity\Page', 'p')
            ->where("p.id = '$pageId'")
            ->getQuery();

        /** @var Page $page */
        $page = $query->getOneOrNullResult();

        if($page != null) {
            $instance = $page->getInstance();
            return array_merge($instance->toArray(),$this->readContentFromInstance($instance));
        }
        else {
            throw new Exception("Page not found.", 404);
        }
    }

    /**
     * Reads the content associated to the given bloc id.
     *
     * @param int $blocId
     * @throws Exception
     * @return array
     */
    public function readFromBlocId($blocId)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('b')
            ->from('app\Model\Entity\Bloc', 'b')
            ->where("b.id = '$blocId'")
            ->getQuery();

        /** @var Bloc $bloc */
        $bloc = $query->getOneOrNullResult();

        if($bloc != null) {
            $instance = $bloc->getInstanceRelation()->getChild();
            return array_merge($instance->toArray(),$this->readContentFromInstance($instance));
        }
        else {
            throw new Exception("Page not found.", 404);
        }
    }

    /**
     * Reads as select field
     *
     * @return array
     */
    public function readAllAsField(){
        /** @var EntityManager $em */
        $em = self::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('i')
            ->from('app\Model\Entity\Instance', 'i')
            ->orderBy('i.position', 'ASC')
            ->getQuery();

        $instances = $query->getResult();

        $field = ["type"=>"select","options"=>[]];

        /** @var Instance $instance */
        foreach($instances as $instance){
            $option = ["name" => $instance->getName(), "value" => $instance->getId()];
            array_push($field["options"], $option);
        }

        return $field;
    }

    private function readContentFromInstance(Instance $instance){
        $values = $instance->getValues();
        $valuesArray = [];
        /** @var Value $value */
        foreach($values as $value){
            $valuesArray[$value->getProperty()->getName()] = $value->getContent();
        }
        $relations = $instance->getRelationsAsChild();
        $relationsArray = [];
        /** @var InstanceRelation $relation */
        foreach($relations as $relation){
            $relationsArray[$relation->getProperty()->getName()] = $relation->getParent()->getId();
        }
        return array_merge($valuesArray,$relationsArray);
    }
}