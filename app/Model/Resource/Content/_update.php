<?php

use Doctrine\ORM\EntityManager;

use app\Model\Entity\Category;
use app\Model\Entity\Property;
use app\Model\Entity\Instance;
use app\Model\Entity\InstanceRelation;
use app\Model\Entity\Value;
use app\Model\Entity\Language;

trait updateContent{
    /**
     * Updates the Content for the given id.
     *
     * @param int $id
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function update($id, $body, $files)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $issues = [];

        $qb = $em->createQueryBuilder();
        $query = $qb->select('i')
            ->from('app\Model\Entity\Instance', 'i')
            ->where("i.id = '$id'")
            ->getQuery();

        /** @var \app\Model\Entity\Instance $instance */
        $instance = $query->getOneOrNullResult();

        if($instance == null) {
            throw new Exception("Instance not found.", 404);
        }

        foreach(["Name","Activation"] as $field){
            try {
                $instance->setField($body, $field, true);
            }
            catch (Exception $e) {
                array_push($issues, $e->getMessage());
            }
        }

        $properties = $instance->getCategory()->getProperties();
        /** @var Property $property */
        foreach($properties as $property){
            if($property->getCategory()!= null && isset($body[$property->getName()])){
                //delete current relation
                /** @var InstanceRelation $relation */
                foreach($instance->getRelationsAsChild() as $relation){
                    if($relation->getProperty() == $property){
                        $em->remove($relation);
                    }
                }
                //create new relation
                $qb = $em->createQueryBuilder();
                $query = $qb->select('i')
                    ->from('app\Model\Entity\Instance', 'i')
                    ->where("i.id = '".$body[$property->getName()]."'")
                    ->getQuery();

                $parentInstance = $query->getOneOrNullResult();
                if($parentInstance !== null){
                    $instanceRelation = new InstanceRelation();
                    $instanceRelation->setChild($instance)
                        ->setParent($parentInstance)
                        ->setProperty($property);


                    $em->persist($instanceRelation);
                }
                else{
                    throw new Exception('Chosen instance for '.$property->getName().' does not exist.');
                }

            }
            elseif($property->getType()!= null && isset($body[$property->getName()])){
                //delete current value
                /** @var Value $value */
                foreach($instance->getValues() as $value){
                    if($value->getProperty() == $property){
                        $em->remove($value);
                    }
                }
                //create new value
                switch($type = $property->getType()->getName()){
                    case "string":
                        if(isset($body[$property->getName()])){
                            $value = new Value;
                            $value->setProperty($property);
                            $value->setContent($body[$property->getName()]);
                            $value->setCreationDate($instance->getCreationDate());
                            $value->setCreationUser($this->getSessionUser());
                            $value->setInstance($instance);
                            $value->setLanguage($em->getRepository('app\Model\Entity\Language')->find(1));

                            $em->persist($value);
                        }
                        break;
                    case "image":

                        break;
                }
            }
        }

        if(!count($issues)) {

            $em->persist($instance);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }


        return $instance->toArray();
    }
}