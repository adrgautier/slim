<?php

use Doctrine\ORM\EntityManager;

use app\Model\Entity\Category;
use app\Model\Entity\Property;
use app\Model\Entity\Instance;
use app\Model\Entity\InstanceRelation;
use app\Model\Entity\Value;
use app\Model\Entity\Language;

trait createContent{
    /**
     * Create user.
     *
     * @param string $category
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function create($category, $body, $files)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();

        $issues = [];

        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('c')
            ->from('app\Model\Entity\Category', 'c')
            ->where("c.name = '$category'")
            ->getQuery();

        /** @var Category $category */
        $category = $query->getOneOrNullResult();

        $instance = new Instance();
        if(isset($body['name']) && $body['name'] !== null){
            //if instance name already exist for this category
            $exist = $category->getInstances()->exists(function($key,Instance $element)use($body){
                if($element->getName() == $body['name']){
                    return true;
                }
                else{
                    return false;
                }
            });
            if($exist){
                throw new Exception('Content name already given.');
            }
            $instance->setName($body['name']);
        }
        else{
            $instance->setName('Instance '.$category->getInstances()->count());
        }
        $instance->setPosition($category->getInstances()->count());
        $instance->setCategory($category);
        if(isset($body['activation']) && $body['activation'] !== null){
            $instance->setActivation($body['activation']);
        }
        else{
            $instance->setActivation(false);
        }
        $instance->setCreationDate(new DateTime());
        $instance->setCreationUser($this->getSessionUser());

        if($category != null) {
            $properties = $category->getProperties();
            /** @var Property $property */
            foreach($properties as $property){
                if($property->getCategory()!= null && isset($body[$property->getName()])){
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('i')
                        ->from('app\Model\Entity\Instance', 'i')
                        ->where("i.id = '".$body[$property->getName()]."'")
                        ->getQuery();

                    $parentInstance = $query->getOneOrNullResult();
                    if($parentInstance !== null){
                        $instanceRelation = new InstanceRelation();
                        $instanceRelation->setChild($instance)
                            ->setParent($parentInstance)
                            ->setProperty($property);


                        $em->persist($instanceRelation);
                    }
                    else{
                        throw new Exception('Chosen instance for '.$property->getName().' does not exist.');
                    }

                }
                elseif($property->getType()!= null && isset($body[$property->getName()])){
                    switch($type = $property->getType()->getName()){
                        case "string":
                            if(isset($body[$property->getName()])){
                                $value = new Value;
                                $value->setProperty($property);
                                $value->setContent($body[$property->getName()]);
                                $value->setCreationDate($instance->getCreationDate());
                                $value->setCreationUser($this->getSessionUser());
                                $value->setInstance($instance);
                                $value->setLanguage($em->getRepository('app\Model\Entity\Language')->find(1));

                                $em->persist($value);
                            }
                            break;
                        case "image":

                            break;
                    }
                }
            }
        }

        if(!count($issues)) {

            $em->persist($instance);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }


        return $instance->toArray();
    }
}