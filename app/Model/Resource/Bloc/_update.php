<?php

trait updateBloc{

    /**
     * Updates the Bloc for the given id.
     *
     * @param int $id
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function update($id, $body, $files)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $issues = [];

        $qb = $em->createQueryBuilder();
        $query = $qb->select('u')
            ->from('app\Model\Entity\Bloc', 'u')
            ->where("u.id = '$id'")
            ->getQuery();

        /** @var \app\Model\Entity\Bloc $bloc */
        $bloc = $query->getOneOrNullResult();

        if($bloc == null) {
            throw new Exception("not found", 404);
        }

        //required fields
        foreach(["Anchor","Activation"] as $field){
            try {
                $bloc->setField($body, $field, true);
            }
            catch (Exception $e) {
                array_push($issues, $e->getMessage());
            }
        }
        try {
            $template = $em->getRepository('app\Model\Entity\BlocTemplate')->find($body['template']);
            $bloc->setTemplate($template);
            $relation = $em->getRepository('app\Model\Entity\InstanceRelation')->find($body['relation']);
            $bloc->setInstanceRelation($relation);
        }
        catch (Exception $e) {
            array_push($issues, $e->getMessage());
        }

        if(!count($issues)) {


            $em->persist($bloc);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }

        return $bloc->toArray();
    }
}