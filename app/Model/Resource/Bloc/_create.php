<?php

use app\Model\Entity\Bloc;

trait createBloc{

    /**
     * Create page.
     *
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function create($body, $files)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $issues = [];

        /** @var Bloc $bloc */
        $bloc = new Bloc();

        //required fields
        foreach(["Anchor","Activation"] as $field){
            try {
                $bloc->setField($body, $field, true);
            }
            catch (Exception $e) {
                array_push($issues, $e->getMessage());
            }
        }
        try {
            $template = $em->getRepository('app\Model\Entity\BlocTemplate')->find($body['template']);
            $bloc->setTemplate($template);
            $relation = $em->getRepository('app\Model\Entity\InstanceRelation')->find($body['relation']);
            $bloc->setInstanceRelation($relation);
            $bloc->setPosition(0);
            $bloc->setLanguage($em->getRepository('app\Model\Entity\Language')->find(1));
        }
        catch (Exception $e) {
            array_push($issues, $e->getMessage());
        }


        if(!count($issues)) {
            $em->persist($bloc);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }

        return $bloc->toArray();
    }
}