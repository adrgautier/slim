<?php

trait deleteBloc{

    /**
     * Deletes the bloc for the given id.
     *
     * @param $id
     * @return array
     * @throws Exception
     */
    public function delete($id){
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $qb = $em->createQueryBuilder();
        $query = $qb->select('u')
            ->from('app\Model\Entity\User', 'u')
            ->where("u.id = '$id'")
            ->getQuery();

        $bloc = $query->getOneOrNullResult();

        if($bloc != null) {
            //remove
            $em->remove($bloc);
            $em->flush();

            return $bloc->toArray();
        }
        else {
            throw new Exception("User not found", 404);
        }

    }

}