<?php

use app\Model\Entity\Bloc;
use Doctrine\ORM\EntityManager;
use app\Model\Entity\Instance;

trait readBloc{


    /**
     * Reads the bloc of the given id.
     *
     * @param $id
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function read($id)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
            ->from('app\Model\Entity\Bloc', 'p')
            ->where("p.id = '$id'")
            ->getQuery();

        /** @var Bloc $bloc */
        $bloc = $query->getOneOrNullResult();

        if($bloc != null) {
            return $bloc->toArray();
        }
        else {
            throw new Exception("Bloc not found", 404);
        }
    }


    /**
     * Reads all the existing blocs.
     *
     * @return array
     */
    public function readAll(){
        /** @var EntityManager $em */
        $em = self::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
            ->from('app\Model\Entity\Bloc', 'p')
            ->orderBy('p.position', 'ASC')
            ->getQuery();

        $blocs = $query->getResult();

        $array = array_map(function (Bloc $bloc) {
            return $bloc->toArray();
        }, $blocs);

        return $array;
    }


    /**
     * Reads all the existing blocs.
     *
     * @return array
     */
    public function readAllFromParentInstanceIdWithCategory($parentInstanceId, $categoryName){
        /** @var EntityManager $em */
        $em = self::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('i')
            ->from('app\Model\Entity\Instance', 'i')
            ->where("i.id = '$parentInstanceId'")
            ->getQuery();

        /** @var Instance $parentInstance */
        $parentInstance = $query->getOneOrNullResult();


        $instanceRelations = $parentInstance->getRelationsAsParent();

        $blocs = [];
        foreach($instanceRelations as $instanceRelation){
            if($instanceRelation->getChild()->getCategory()->getName() == $categoryName){
                array_push($blocs, $instanceRelation->getBloc());
            }
        }

        $array = array_map(function (Bloc $bloc) {
            return $bloc->toArray();
        }, $blocs);

        return $array;
    }

}