<?php

trait updateUser{

    /**
     * Updates the User for the given id.
     *
     * @param int $id
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function update($id, $body, $files)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $issues = [];

        $qb = $em->createQueryBuilder();
        $query = $qb->select('u')
            ->from('app\Model\Entity\User', 'u')
            ->where("u.id = '$id'")
            ->getQuery();

        /** @var \app\Model\Entity\User $user */
        $user = $query->getOneOrNullResult();

        if($user == null) {
            throw new Exception("not found", 404);
        }

        //required fields
        foreach(["Name","Mail"] as $field){
            try {
                $user->setField($body, $field, true);
            }
            catch (Exception $e) {
                array_push($issues, $e->getMessage());
            }
        }

        //password field
        if(isset($body["password"])){
            $password = json_decode($body["password"], true);
            if(isset($password["current"]) && $password["current"] !== null && $password["current"] !== ""){
                try {
                    $user->setPassword($body["password"]);
                }
                catch (Exception $e) {
                    array_push($issues, $e->getMessage());
                }
            }
        }

        if(!count($issues)) {

            //optional fields
            foreach(["Role"] as $field){
                $user->setField($body, $field, false);
            }

            $em->persist($user);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }

        //file handler*/
        if(isset($files["avatar"])) {

            $id = $user->getId();
            $usersContentsFolder = \app\shared\Helper::getAppDir() . "/../www/contents/admin/user/";
            $userContentsFolder = $usersContentsFolder . $id . "/";
            if (!is_dir($userContentsFolder)) {
                mkdir($userContentsFolder);
            }

            /** @var \Psr\Http\Message\UploadedFileInterface $avatar */
            $avatar = $files["avatar"];

            //set avatar hash
            $user->setAvatar($avatar);

            //move avatar file
            $imageExtension = pathinfo($avatar->getClientFileName(), PATHINFO_EXTENSION);
            $avatar->moveTo($userContentsFolder . "avatar.$imageExtension");

            //save
            $em->flush();

        }

        return $user->toArray();
    }
}