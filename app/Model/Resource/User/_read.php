<?php

use app\Model\Entity\User;

trait readUser{

    /**
     * Reads the user of the given id.
     *
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public function read($id)
    {
        //$user = parent::getEntityManager()->find(self::ENTITY, $id);
        //doesn't work
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = parent::getEntityManager()->createQueryBuilder();
        $query = $qb->select('u')
            ->from('app\Model\Entity\User', 'u')
            ->where("u.id = '$id'")
            ->getQuery();

        $user = $query->getOneOrNullResult();

        if($user != null) {
            return $user->toArray();
        }
        else {
            throw new Exception("not found", 404);
        }
    }

    /**
     * Reads the user of the given mail.
     *
     * @param string $mail
     * @return array
     * @throws \Exception
     */
    public function readFromMail($mail){
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = parent::getEntityManager()->createQueryBuilder();

        $query = $qb->select('u')
            ->from('app\Model\Entity\User', 'u')
            ->where("u.mail = '$mail'")
            ->getQuery();

        $user = $query->getOneOrNullResult();

        if($user != null) {
            return $user->toArray();
        }
        else {
            throw new Exception("no match", 404);
        }
    }

    /**
     * Read all the existing users.
     *
     * @return array
     */
    public function readAll(){
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = parent::getEntityManager()->createQueryBuilder();

        $query = $qb->select('u')
            ->from('app\Model\Entity\User', 'u')
            ->orderBy('u.name', 'ASC')
            ->getQuery();

        $users = $query->getResult();

        $array = array_map(function (User $user) {
            return $user->toArray();
        },
            $users);
        return $array;
    }

}