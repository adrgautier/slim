<?php

use app\Model\Entity\User;

trait createUser{

    /**
     * Create user.
     *
     * @param array $body
     * @param array $files
     * @return string
     * @throws Exception
     */
    public function create($body, $files)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $issues = [];

        /** @var User $user */
        $user = new User();

        //required fields
        foreach(["Name","Mail","Password"] as $field){
            try {
                $user->setField($body, $field, true);
            }
            catch (Exception $e) {
                array_push($issues, $e->getMessage());
            }
        }

        if(!count($issues)) {

            //optional fields
            foreach(["Role"] as $field){
                $user->setField($body, $field, false);
            }

            $em->persist($user);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }

        //file handler
        if(isset($files["avatar"])) {

            $id = $user->getId();
            $usersContentsFolder = \app\shared\Helper::getAppDir() . "/../www/contents/admin/user/";
            $userContentsFolder = $usersContentsFolder . $id . "/";
            if (!is_dir($userContentsFolder)) {
                mkdir($userContentsFolder);
            }

            /** @var \Psr\Http\Message\UploadedFileInterface $avatar */
            $avatar = $files;

            //set avatar hash
            $user->setAvatar($avatar);

            //move avatar file
            $imageExtension = pathinfo($avatar->getClientFileName(), PATHINFO_EXTENSION);
            $avatar->moveTo($userContentsFolder . "avatar.$imageExtension");

            //save
            $em->flush();

        }

        return $user->toArray();
    }
}