<?php

trait deleteUser{

    /**
     * Deletes the user for the given id.
     *
     * @param $id
     * @return array
     * @throws Exception
     */
    public function delete($id){
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = parent::getEntityManager();

        $qb = $em->createQueryBuilder();
        $query = $qb->select('u')
            ->from('app\Model\Entity\User', 'u')
            ->where("u.id = '$id'")
            ->getQuery();

        $user = $query->getOneOrNullResult();

        if($user != null) {
            //remove
            $em->remove($user);
            $em->flush();

            return $user->toArray();
        }
        else {
            throw new Exception("User not found", 404);
        }

    }

}