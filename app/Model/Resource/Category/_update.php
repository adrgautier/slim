<?php

use app\Model\Entity\Property;
use app\Model\Entity\Category;
use Doctrine\ORM\EntityManager;

trait updateCategory{

    /**
     * Update the category of the given id.
     *
     * @param int $id
     * @param array $body
     * @return string
     * @throws Exception
     */
    public function update($id, $body)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $issues = [];

        $qb = $em->createQueryBuilder();
        $query = $qb->select('c')
            ->from('app\Model\Entity\Category', 'c')
            ->where("c.id = '$id'")
            ->getQuery();

        /** @var Category $category */
        $category = $query->getOneOrNullResult();

        if($category == null) {
            throw new Exception("Category not found.", 404);
        }

        //update existing properties
        $properties = json_decode($body["properties"],true);

        foreach($properties as $property)
        {
            /** @var \app\Model\Entity\Property $currentProperty */
            $qb = $em->createQueryBuilder();
            $query = $qb->select('p')
                ->from('app\Model\Entity\Property', 'p')
                ->where("p.id = '".$property["id"]."'")
                ->getQuery();

            $currentProperty = $query->getOneOrNullResult();

            //$currentProperty = $em->getRepository('app\Model\Entity\Property')->findOneById($property["id"]);


            if($currentProperty != null) {
                $currentProperty->setName($property["name"]);
                $currentProperty->setActivation($property["activation"]);


                $id = $property["mode"]["id"];

                switch($property["mode"]["entity"]){
                    case "type":
                        $qb = $em->createQueryBuilder();
                        $query = $qb->select('t')
                            ->from('app\Model\Entity\Type', 't')
                            ->where("t.id = '$id'")
                            ->getQuery();

                        $type = $query->getOneOrNullResult();

                        if($type != null) {
                            $currentProperty->setType($type)
                                ->setCategory();
                        }
                        else {
                            throw new Exception("Type of id:$id not found.", 404);
                        }
                        break;
                    case "category":
                        $qb = $em->createQueryBuilder();
                        $query = $qb->select('c')
                            ->from('app\Model\Entity\Category', 'c')
                            ->where("c.id = '$id'")
                            ->getQuery();

                        $cat = $query->getOneOrNullResult();

                        if($cat != null) {
                            $currentProperty->setCategory($cat)
                                ->setType();
                        }
                        else{
                            throw new Exception("Category of id:$id not found.", 404);
                        }
                        break;
                }
                $em->persist($currentProperty);
            }
            else {
                throw new Exception("Property of id:".$property["id"]." not found.", 404);
            }
        }

        //delete existing properties
        $deletedProperties = json_decode($body["deletedProperties"],true);
        foreach($deletedProperties as $deletedProperty){
            $qb = $em->createQueryBuilder();
            $query = $qb->select('p')
                ->from('app\Model\Entity\Property', 'p')
                ->where("p.id = '".$deletedProperty["id"]."'")
                ->getQuery();

            $property = $query->getOneOrNullResult();

            if($property != null) {
                $em->remove($property);
            }
            else {
                throw new Exception("Property of id:".$deletedProperty["id"]." not found", 404);
            }

        }

        //create new properties
        $createdProperties = json_decode($body["createdProperties"],true);

        foreach($createdProperties as $property)
        {
            /** @var Property $createdProperty */
            $createdProperty = new Property();
            $createdProperty->setName($property["name"])
                            ->setActivation(true)
                            ->setPosition($category->getProperties()->count())
                            ->setParentCategory($category);

            $id = $property["mode"]["id"];
            switch($property["mode"]["entity"]){
                case "type":
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('t')
                        ->from('app\Model\Entity\Type', 't')
                        ->where("t.id = '$id'")
                        ->getQuery();

                    $type = $query->getOneOrNullResult();

                    if($type != null) {
                        $createdProperty->setType($type);
                    }
                    else {
                        throw new Exception("Type of id:$id not found.", 404);
                    }
                    break;
                case "category":
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('c')
                        ->from('app\Model\Entity\Category', 'c')
                        ->where("c.id = '$id'")
                        ->getQuery();

                    $cat = $query->getOneOrNullResult();

                    if($cat != null) {
                        $createdProperty->setCategory($cat);
                    }
                    else {
                        throw new Exception("Category of id:$id not found.", 404);
                    }
                    break;
            }

            $em->persist($createdProperty);

        }

        if(!count($issues)) {
            $em->persist($category);
            $em->flush();
        }
        else{
            throw new Exception(json_encode($issues));
        }


        return $category->toArray();
    }
}