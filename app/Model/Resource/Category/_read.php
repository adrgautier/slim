<?php

use app\Model\Entity\Category;
use app\Model\Entity\Property;
use app\Model\Entity\Instance;
use Doctrine\ORM\EntityManager;

trait readcategory{

    /**
     * Reads the category of the given id.
     *
     * @param $id
     * @throws Exception
     * @return array
     */
    public function read($id)
    {
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('c')
            ->from('app\Model\Entity\Category', 'c')
            ->where("c.id = '$id'")
            ->getQuery();

        /** @var Category $category */
        $category = $query->getOneOrNullResult();

        if($category != null) {
            $properties = $category->getProperties();
            $propertiesArray = [];
            foreach($properties as $property){
                $mode = [];
                if($property->getType() != null){
                    $mode = $property->getType()->toArray();
                    $mode["entity"] = "type";
                }
                elseif($property->getCategory() != null){
                    $mode = $property->getCategory()->toArray();
                    $mode["entity"] = "category";
                }
                array_push($propertiesArray,
                    array_merge($property->toArray(), ["mode" => $mode])
                );
            }
            return array_merge($category->toArray(),["properties" => $propertiesArray]);
        }
        else {
            throw new Exception("not found", 404);
        }
    }

    /**
     * Reads all the existing categories.
     *
     * @return array
     */
    public function readAll(){
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('c')
            ->from('app\Model\Entity\Category', 'c')
            ->orderBy('c.position', 'ASC')
            ->getQuery();

        $categories = $query->getResult();

        $array = array_map(function (Category $category) {
            return $category->toArray();
        },
            $categories);

        return $array;
    }

    /**
     * Find all fields of the form associated to the given category.
     * The fields are generated according to the category's properties.
     *
     * @param string $name
     * @throws Exception
     * @return array
     */
    public function readFieldsFromName($name){
        /** @var EntityManager $em */
        $em = parent::getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('c')
            ->from('app\Model\Entity\Category', 'c')
            ->where("c.name = '$name'")
            ->getQuery();

        /** @var Category $category */
        $category = $query->getOneOrNullResult();

        if($category != null) {
            $fields = [];
            //build the fields
            $properties = $category->getProperties();
            /** @var Property $property */
            foreach($properties as $property){
                $field = [];
                if($property->getCategory()!= null){
                    $instances = [];
                    /** @var Instance $instance */
                    foreach($property->getCategory()->getInstances() as $instance){
                        array_push($instances, ["name" => $instance->getName(), "value" => $instance->getId()]);
                    }
                    $field["type"] = "select";
                    $field["options"] = $instances;
                }
                elseif($property->getType()!= null){
                    switch($type = $property->getType()->getName()){
                        case "string":
                        case "image":
                            $field["type"] = $type;
                            break;
                    }
                }
                if(count($field)>0){
                    $fields[$property->getName()]=$field;
                }
            }

            return $fields;
        }
        else {
            throw new Exception("not found", 404);
        }
    }
}