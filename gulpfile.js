//add to Prototype
String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

var camelCase = function(array){
    var camelString = "",
        refkey= 0;
    array.forEach(function(string, key){
        if(string !== ""){
            if(key>refkey){
                camelString += string.capitalizeFirstLetter();

            }
            else{
                camelString += string;
            }
        }
        else{
            refkey++;
        }
    });
    return camelString;
};

//Gulp Requirements
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    del = require('del'),
    pleeease = require('gulp-pleeease'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    preprocess = require('gulp-preprocess');

var Eyeglass = require("eyeglass");

var eyeglass = new Eyeglass({
    // ... node-sass options
    importer: function(uri, prev, done) {
        done(sass.compiler.types.NULL);
    }
});

// Disable import once with gulp until we
// figure out how to make them work together.
eyeglass.enableImportOnce = false;

var SassOptions = eyeglass.sassOptions();

var PleeeaseOptions = {
    "autoprefixer": true,
    "minifier": true
},
    PleeeaseOptionsDev = {
        "autoprefixer": true,
        "minifier": false
    };

//Yaml file loading Requierments
var yaml = require('js-yaml'),
    fs   = require('fs');

// Get gulp conf, or throw exception on error
var gulpconfig;
try {
    gulpconfig = yaml.safeLoad(fs.readFileSync('app/conf/gulp.yml', 'utf8'));
} catch (e) {
    gulpconfig = { "error" : e };
}

//Get scheme
var scheme = {};
if(gulpconfig.scheme){
    try {
        scheme = yaml.safeLoad(fs.readFileSync(gulpconfig.scheme, 'utf8'));
    } catch (e) {
        scheme = { "error" : e };
    }
}
gulp.task("scheme", function() {
    console.log(scheme);
    return gulp.src("app/View/shared/_base16.preprocess.scss")
        .pipe(preprocess({context: scheme}))
        .pipe(rename("_base16.scss"))
        .pipe(gulp.dest("app/View/Admin/assets/scss/"));

});


var jstasks = [],
    jstasksdev = [],
    jstasker = function(bundlename,filecat,subcat,src){
        var taskname = "js_"+bundlename+"_"+subcat+filecat,
            filename = camelCase([subcat,filecat]);

        (function(taskname,filename,bundlename,src){
            gulp.task(taskname, function() {
                return gulp.src(src)
                    .pipe(uglify())
                    .pipe(concat(filename+'.min.js'))
                    .pipe(gulp.dest('www/assets/'+bundlename+'/js/'));
            });

            gulp.task(taskname+"_dev", function() {
                return gulp.src(src)
                    .pipe(concat(filename+'.js'))
                    .pipe(gulp.dest('www/assets_dev/'+bundlename+'/js/'));
            });
        })(taskname,filename,bundlename,src);

        // Add taskname to jstasks array
        jstasks.push(taskname);
        jstasksdev.push(taskname+"_dev");
    };

var csstasks = [],
    csstasksdev = [],
    csstasker = function(bundlename,filecat,subcat,src){
        var taskname = "css_"+bundlename+"_"+subcat+filecat,
            filename = camelCase([subcat,filecat]);

        (function(taskname,filename,bundlename,src){
            gulp.task(taskname, function() {
                return gulp.src(src)
                    .pipe(sass(SassOptions).on('error', sass.logError))
                    .pipe(pleeease(PleeeaseOptions))
                    .pipe(concat(filename+'.min.css'))
                    .pipe(gulp.dest('www/assets/'+bundlename+'/css/'));
            });

            gulp.task(taskname+"_dev", function() {
                return gulp.src(src)
                    .pipe(sass(SassOptions).on('error', sass.logError))
                    .pipe(pleeease(PleeeaseOptionsDev))
                    .pipe(concat(filename+'.css'))
                    .pipe(gulp.dest('www/assets_dev/'+bundlename+'/css/'));
            });
        })(taskname,filename,bundlename,src);

        // Add taskname to csstasks array
        csstasks.push(taskname);
        csstasksdev.push(taskname+"_dev");
    };

var fonttasks = [],
    fonttasksdev = [],
    fonttasker = function(bundlename,src){
        var taskname = "font_"+bundlename;

        (function(taskname,bundlename,src){
            gulp.task(taskname, function() {
                return gulp.src(src)
                    .pipe(gulp.dest('www/assets/'+bundlename+'/fonts/'));
            });

            gulp.task(taskname+"_dev", function() {
                return gulp.src(src)
                    .pipe(gulp.dest('www/assets_dev/'+bundlename+'/fonts/'));
            });
        })(taskname,bundlename,src);

        // Add taskname to fonttasks array
        fonttasks.push(taskname);
        fonttasksdev.push(taskname+"_dev");
    };

var imgtasks = [],
    imgtasksdev = [],
    imgtasker = function(bundlename,src){
        var taskname = "img_"+bundlename;

        (function(taskname,bundlename,src){
            gulp.task(taskname, function() {
                return gulp.src(src)
                    .pipe(gulp.dest('www/assets/'+bundlename+'/img/'));
            });

            gulp.task(taskname+"_dev", function() {
                return gulp.src(src)
                    .pipe(gulp.dest('www/assets_dev/'+bundlename+'/img/'));
            });
        })(taskname,bundlename,src);

        // Add taskname to imgtasks array
        imgtasks.push(taskname);
        imgtasksdev.push(taskname+"_dev");
    };

for(var bundlename in gulpconfig.paths){
    var bundle = gulpconfig.paths[bundlename];

    //js
    for(var filecat in bundle.js){
        var element = bundle.js[filecat];
        if(typeof element == "string" || Array.isArray(element)){
            jstasker(bundlename, filecat, "", element);
        }
        else{
            for(var subcat in element){
                var subelem = element[subcat];
                jstasker(bundlename, filecat, subcat, subelem);
            }
        }
    }

    //css
    for(var filecat in bundle.css){
        var element = bundle.css[filecat];
        if(typeof element == "string" || Array.isArray(element)){
            csstasker(bundlename, filecat, "", element);
        }
        else{
            for(var subcat in element){
                var subelem = element[subcat];
                csstasker(bundlename, filecat, subcat, subelem);
            }
        }
    }

    //font
    if(bundle.fonts){
        fonttasker(bundlename,bundle.fonts);
    }

    //img
    if(bundle.img){
        imgtasker(bundlename,bundle.img);
    }
}

gulp.task('clean', function() {
    return del(['www/assets','www/assets_dev']);
});

gulp.task('js_dump', jstasks);
gulp.task('css_dump', csstasks);
gulp.task('font_dump', fonttasks);
gulp.task('img_dump', imgtasks);
gulp.task('dump', ['js_dump','css_dump']);


gulp.task('js_dump_dev', jstasksdev);
gulp.task('css_dump_dev', csstasksdev);
gulp.task('font_dump_dev', fonttasksdev);
gulp.task('img_dump_dev', imgtasksdev);
gulp.task('dump_dev', ['js_dump_dev','css_dump_dev']);
gulp.task('watch_dev', function(){
    gulp.watch(['app/**/*.twig','app/**/*.scss','app/**/*.js'],['dump_dev']);
});