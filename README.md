#MÉTADMIN
##Ressources utilisées :
####Modules nodeJS
- bower
- del
- gulp
- gulp-concat
- gulp-pleeease
- gulp-preprocess
- gulp-rename
- gulp-sass
- gulp-uglify
- js-yaml

####PHP
- Slim (v3) : micro framework PHP
- Twig pour Slim
- Protection CSRF pour Slim
- Gestion des sessions pour Slim 
- Doctrine
- Yaml (parseur de symfony)
- Minify
- Image

####JS
- angular
- angular-animate
- angular-cookies
- angular-file-model
- angular-ui-router

####SASS
- neutroncss
- material-shadows
- sanitize-css

####Autre
- material-design-iconic-font
- base16-schemes

##Mise en place :

####1. Télécharger les dépendances nodeJS
```
npm update
```

####2. Téléchargement des librairies PHP via composer (non inclut)
```
composer update
```

####3. Téléchargement des librairies JS/CSS via bower
```
bower update
```

##Commandes Gulp :

Suffixez les commandes avec `_dev` en mode developpement.

####Génération des assets
```
npm run gulp dump(_dev)
```

####Génération automatique des assets (en mode developpement uniquement)
```
npm run gulp watch_dev
```

####Déplacement des fichiers de police
```
npm run gulp font_dump(_dev)
```

##Commandes utiles :

####Générer les getter/setter
```
vendor/bin/doctrine orm:generate-entities .
```
####Mettre à jour la base de données
```
vendor/bin/doctrine orm:schema-tool:update --force
```

####Convertir les définitions du Model (en yml)
```
vendor/bin/doctrine orm:convert:mapping yml app/Model/Entity
```

####Mettre à jour l'autoloader de Class
```
sudo php ../composer.phar dump-autoload
```


####Prise en compte dans Git des fichiers ignorés
```
git rm -rf --cached .
git add .
```

##PHP Documentor :

####Générer la documentation
```
phpdoc -d ./app -t ./docs
```

####Rendre la documentation accessible
Création d'un lien symbolique de la documentation dans le dossier publique **www**.
```
ln -s ../docs ./www
```

####Génération de l'arborescence des classes
La génération de ce graphique nécessite l'installation de GraphViz (`dot -v` pour vérifier sa présence).
```
sudo apt-get install graphviz
```